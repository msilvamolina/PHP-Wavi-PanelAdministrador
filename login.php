<?php include('../paginas_include/variables-generales.php'); 

session_start();
if($_SESSION) {
    $redireccion = $Servidor_url.'PANELADMINISTRADOR/'; 
    header('Location: '.$redireccion);
    exit;
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Wavi Administrador</title>
  <meta name="author" content="Martín Silva Molina">
    <meta name="HandheldFriendly" content="True">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="cleartype" content="on">

  <link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/reset.css"> <!-- CSS reset -->
        <link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/font-awesome.min.css" />
      <link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/nuevohome.css">
    <script src="<?php echo $Servidor_url; ?>js/variables-generales.js"></script>            
    <script src="<?php echo $Servidor_url; ?>js/modernizr.js"></script> <!-- Modernizr -->
    <link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/login.css"> <!-- Gem style -->
        <link rel="stylesheet" href="<?php echo $Servidor_url; ?>css/form.css"> <!-- Gem style -->

     <script src="<?php echo $Servidor_url; ?>js/jquery-2.1.1.js"></script>
        <style type="text/css">
        #btn_login {
          background-color: #FF9800 !important;
        }
        .error {
          background: #4527A0;
          padding: 13px;
          color: #ffffff;
          text-align: center;
        }


        </style>
</script>
</head>
<body>
  <div class="parte_abajo">
  <img src="<?php echo $Servidor_url; ?>img/logo_nuevo.png" class="logo_paradigma">
          <div class="formulario_adentro"> <!-- log in form -->
          <form class="cd-form" method="post" action="<?php echo $Servidor_url;?>PANELADMINISTRADOR/php/loguear-usuario.php">
              <?php if($_GET['error']) { ?>
            <div class="error">
              <p>Los datos ingresados son incorrectos</p>
            </div>
          <?php } ?>
              <p class="fieldset">
                <label class="image-replace cd-username" for="signin-email">Usuario o E-mail</label>
                <input class="full-width has-padding has-border"  name="usuario" required id="signin-email" type="text" placeholder="Usuario o E-mail">
              </p>
             <p class="fieldset">
              <label class="image-replace cd-password" for="signin-password">Contraseña</label>
              <input class="full-width has-padding has-border" name="contrasena" required id="signin-password"  type="password"  placeholder="Contraseña">
              <a href="#0" class="hide-password">Mostrar</a>
            </p>
        <div>
            <input type="hidden" name="redirigir" value="<?php echo trim($_GET['redirigir']); ?>"/>
        </div>
            <p class="fieldset">
        <div class="contenedor_boton">
          <p class="fieldset">
            <input class="full-width" type="submit" id="btn_login" value="Continuar">
          </p>
        </div>              
            </p>
            <br>
          </form>
                    <!-- <a href="#0" class="cd-close-form">Close</a> -->
        </div> <!-- cd-login -->  
    <div class="clear"></div>

  </div>
     <script src="<?php echo $Servidor_url; ?>js/jquery-2.1.1.js"></script>

<script src="<?php echo $Servidor_url; ?>js/login.js"></script>

    </body>
</html>
