<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$tipo = "negocios";


conectar2('mywavi', 'WAVI');

//consultar en la base de datos
$query_rs_grupo_categoria = "SELECT id_grupo_categoria, categoria_nombre, categoria_imagen FROM grupo_categorias WHERE categoria_tipo = '$tipo' ORDER BY categoria_nombre ASC";
$rs_grupo_categoria = mysql_query($query_rs_grupo_categoria)or die(mysql_error());
$row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria);
$totalrow_rs_grupo_categoria = mysql_num_rows($rs_grupo_categoria);

$tipo = "servicios";

//consultar en la base de datos
$query_rs_servicios = "SELECT id_grupo_categoria, categoria_nombre, categoria_imagen FROM grupo_categorias WHERE categoria_tipo = '$tipo' ORDER BY categoria_nombre ASC";
$rs_servicios = mysql_query($query_rs_servicios)or die(mysql_error());
$row_rs_servicios = mysql_fetch_assoc($rs_servicios);
$totalrow_rs_servicios = mysql_num_rows($rs_servicios);

$tipo = "profesionales";

//consultar en la base de datos
$query_rs_profesionales = "SELECT id_grupo_categoria, categoria_nombre, categoria_imagen FROM grupo_categorias WHERE categoria_tipo = '$tipo' ORDER BY categoria_nombre ASC";
$rs_profesionales = mysql_query($query_rs_profesionales)or die(mysql_error());
$row_rs_profesionales = mysql_fetch_assoc($rs_profesionales);
$totalrow_rs_profesionales = mysql_num_rows($rs_profesionales);

desconectar();

$url_imagen = $Servidor_url."APLICACION/Imagenes/categorias/grandes/";

?>
<!DOCTYPE html>
<html>
<head>
	<?php include('../../includes/head-general.php'); ?>

	<style type="text/css">
		/* Style the tab */
		* {
			padding: 0;
			margin: 0;
		}
		body {
			background: #424242;
		}
		div.tab {
			overflow: hidden;
			background-color: #D32F2F;

		}

		/* Style the links inside the tab */
		div.tab a {
			font-size: 15px;
			color: #fff;
			float: left;
			display: block;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
			transition: 0.3s;
		}

		/* Change background color of links on hover */
		div.tab a:hover {
			background-color: #E57373;
		}

		/* Create an active/current tablink class */
		div.tab a:focus, .active {
			background-color: #E57373;
		}

		/* Style the tab content */
		.tabcontent {
			display: none;
			border-top: none;
		}
		header { 
			position: fixed;
			width: 100%;
		}
		.header {
			width: 100%;
			height: 40px;
			padding: 10px;
			background: #F44336;
		}

		.header img {
			width: 60px;
			margin: 0 auto !important;
		}

		.tabcontent .categoria {
			padding: 10px;
			width: 100px;
			height: 100px;
			float:left;
		}
		.tabcontent img{
			width: 60px;
			margin: 0 auto;
		}
		.tabcontent p {
			color: #fff;
			font-style: normal;
			padding: 2px;
			margin-top: 5px;
			text-align: center;
			font-size: 12px;
			overflow:hidden; /* Escondemos la parte sobrante */
			white-space:wrap; /* Indicamos que no realice salto de linea si no cabe en la anchura indicada */
			text-overflow: ellipsis; /* Ponemos los dos puntos */	
		}

		.contenido_general {
			padding-top: 80px;
			padding-bottom: 40px;
		}
	</style>
</head>
<body>
	<header>
		<div class="header">
			<center>
				<img src="<?php echo $Servidor_url; ?>img/logo_blanco.png" />
			</center>
		</div>
		<div class="tab">

			<a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Servicios')">Servicios</a>
			<a href="javascript:void(0)" class="tablinks active" onclick="openCity(event, 'Negocios')">Negocios</a>
			<a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Profesionales')">Profesionales</a>
		</div>
	</header>
	<div class="contenido_general">
		<div id="Servicios" class="tabcontent">
			<?php do { 
				$id_grupo_categoria = $row_rs_servicios['id_grupo_categoria'];
				$categoria_nombre = $row_rs_servicios['categoria_nombre'];
				$categoria_imagen = $row_rs_servicios['categoria_imagen']; 

				$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
				$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
				if($categoria_imagen) {
					$imagen = $url_imagen.$categoria_imagen;
					$nombre_imagen = $categoria_imagen;
				}
				?>

				<a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/emulador/ficha-categoria.php?categoria=<?php echo $id_grupo_categoria; ?>">
					<div class="categoria">
						<center>
							<img src="<?php echo $imagen; ?>" />
						</center>
						<p><?php echo $categoria_nombre; ?></p>
					</div>
				</a>
				<?php } while($row_rs_servicios = mysql_fetch_assoc($rs_servicios)); ?>	 
			</div>

			<div id="Negocios" class="tabcontent">
				<?php do { 
					$id_grupo_categoria = $row_rs_grupo_categoria['id_grupo_categoria'];
					$categoria_nombre = $row_rs_grupo_categoria['categoria_nombre'];
					$categoria_imagen = $row_rs_grupo_categoria['categoria_imagen']; 

					$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
					$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
					if($categoria_imagen) {
						$imagen = $url_imagen.$categoria_imagen;
						$nombre_imagen = $categoria_imagen;
					}
					?>

					<a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/emulador/ficha-categoria.php?categoria=<?php echo $id_grupo_categoria; ?>">
						<div class="categoria">
							<center>
								<img src="<?php echo $imagen; ?>" />
							</center>
							<p><?php echo $categoria_nombre; ?></p>
						</div>
					</a>
					<?php } while($row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria)); ?>	 
				</div>

				<div id="Profesionales" class="tabcontent">
					<?php do { 
						$id_grupo_categoria = $row_rs_profesionales['id_grupo_categoria'];
						$categoria_nombre = $row_rs_profesionales['categoria_nombre'];
						$categoria_imagen = $row_rs_profesionales['categoria_imagen']; 

						$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
						$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
						if($categoria_imagen) {
							$imagen = $url_imagen.$categoria_imagen;
							$nombre_imagen = $categoria_imagen;
						}
						?>

						<a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/emulador/ficha-categoria.php?categoria=<?php echo $id_grupo_categoria; ?>">
							<div class="categoria">
								<center>
									<img src="<?php echo $imagen; ?>" />
								</center>
								<p><?php echo $categoria_nombre; ?></p>
							</div>
						</a>
						<?php } while($row_rs_profesionales = mysql_fetch_assoc($rs_profesionales)); ?>	 
					</div>
				</p>
			</div>
			<script type="text/javascript">
				openCity(event, 'Negocios');

				function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
    	tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
    	tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>
</body>
</html>