<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$id_categoria = trim($_POST['id_categoria']);
$categoria_nombre = trim($_POST['categoria_nombre']);

$acentos = array('á', 'é', 'í', 'ó', 'ú');
$no_acentos = array('a', 'e', 'i', 'o', 'u');

$nuevo_nombre = quitar_acentos($categoria_nombre);

$nuevo_nombre = strtolower($nuevo_nombre);
$nuevo_nombre = str_replace(' ', '_', $nuevo_nombre);

$array_eliminar = array('"', "'");
$nuevo_nombre = str_replace($array_eliminar, '', $nuevo_nombre);
$nuevo_nombre = str_replace($acentos, $no_acentos, $nuevo_nombre);


$nuevo_nombre = $id_categoria.'-'.rand().'-'.$nuevo_nombre;

$storeFolder = '../../../../APLICACION/Imagenes/categorias/temp/';

if (!empty($_FILES)) {
	$tempFile = $_FILES['file']['tmp_name'];         

	$explorar_extension = explode('.', $_FILES['file']['name']);
	$extension = end($explorar_extension);
	$nuevo_nombre = $nuevo_nombre.'.'.$extension;
	$targetFile =  $storeFolder. $nuevo_nombre; 

	move_uploaded_file($tempFile,$targetFile);

	conectar2('mywavi', 'WAVI');
	mysql_query("UPDATE grupo_categorias SET categoria_imagen='$nuevo_nombre' WHERE id_grupo_categoria='$id_categoria'");
	desconectar();
}
?>