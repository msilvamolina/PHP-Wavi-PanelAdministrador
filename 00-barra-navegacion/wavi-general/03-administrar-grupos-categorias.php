<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

//eliminamos los temporales

conectar2('mywavi', 'WAVI');

//consultar en la base de datos
$query_rs_grupo_categoria = "SELECT id_grupo_categoria, categoria_nombre, categoria_imagen, categoria_tipo FROM grupo_categorias ORDER BY categoria_tipo ASC,categoria_nombre ASC";
$rs_grupo_categoria = mysql_query($query_rs_grupo_categoria)or die(mysql_error());
$row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria);
$totalrow_rs_grupo_categoria = mysql_num_rows($rs_grupo_categoria);


//consultar en la base de datos
$query_rs_categorias = "SELECT id_categoria, id_grupo_categoria, id_subgrupo_categoria FROM categorias";
$rs_categorias = mysql_query($query_rs_categorias)or die(mysql_error());
$row_rs_categorias = mysql_fetch_assoc($rs_categorias);
$totalrow_rs_categorias = mysql_num_rows($rs_categorias);

do {
	$id_categoria = $row_rs_categorias['id_categoria'];
	$id_grupo_categoria = $row_rs_categorias['id_grupo_categoria'];
	$id_subgrupo_categoria = $row_rs_categorias['id_subgrupo_categoria'];

	if($id_grupo_categoria) {
		$array_grupo[$id_grupo_categoria]++;
	} else {
		$categorias_sin_grupo++;
	}

	if($id_subgrupo_categoria) {
		$array_subgrupo[$id_grupo_categoria]++;
	} else {
		$categorias_sin_subgrupo++;
	}
} while($row_rs_categorias = mysql_fetch_assoc($rs_categorias));

if(!$categorias_sin_grupo) {
	$categorias_sin_grupo = 0;
}

if(!$categorias_sin_subgrupo) {
	$categorias_sin_subgrupo = 0;
}

//consultar en la base de datos
$query_rs_subgrupo = "SELECT id_subgrupo, subgrupo_nombre  FROM subgrupos_categorias ";
$rs_subgrupo = mysql_query($query_rs_subgrupo)or die(mysql_error());
$row_rs_subgrupo = mysql_fetch_assoc($rs_subgrupo);
$totalrow_rs_subgrupo = mysql_num_rows($rs_subgrupo);

desconectar();

$url_imagen = $Servidor_url."APLICACION/Imagenes/categorias/grandes/";
?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<script type="text/javascript" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jszip.js"></script>
	<script type="text/javascript" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jszip-utils.js"></script>
	<script type="text/javascript" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/FileSaver.js"></script>

	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}

		.tabla {
			width: 100%;
		}
		.tabla tr td{
			padding: 10px;
		}	

		.tabla tr:nth-of-type(2n) {
			background: #f5e5f2;
		}
		.no_hay_imagen{
			color: #acacac;
		}
		.tabla_encabezado {
			color: red;
		}

		tr {
			cursor: pointer;
		}

		.categorias_con_subgrupos {
			background: #f90 !important;
			color: #fff !important;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->
			<div class="cd-form floating-labels" style="max-width:1600px">
				<div style="max-width:600px; margin:0 auto;">
					<section id="crear_categoria" >							
						<fieldset >
							<form action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/php/03-agregar-grupo-categoria-db.php" method="post">
								<legend id="txt_nueva_categoria">Nuevo grupo</legend>	
								<div class="icon">
									<label class="cd-label" for="cd-company">Nombre del grupo</label>
									<input class="company" type="text" name="nombre_grupo" id="nueva_categoria_nombre" autofocus required>
								</div> 
								<p class="cd-select">
									<select name="tipo" class="select_class" required>
										<option value="">Tipo de categoría</option>
										<option value="negocios">Negocios</option>	
										<option value="servicios">Servicios</option>
										<option value="profesionales">Profesionales</option>		
									</select></p>
									<div class="alinear_centro">
										<button class="boton_azul" id="btn_continuar" >Cargar</button>
									</div>


								</form>
							</fieldset>	
						</section>    	
					</div>	


					<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-administrador/03-categorias-todas.php">
						<h3><b><?php echo $totalrow_rs_categorias; ?></b> categorías en total / <b><?php echo $categorias_sin_grupo; ?></b> categorías sin vincular / <b><?php echo $categorias_sin_subgrupo; ?></b> categorías sin subgrupo</h3>
					</a>
					<br>
					<p><b><?php echo $totalrow_rs_grupo_categoria; ?></b> grupos de categorías</p>		
					<br>
					<p><b><?php echo $totalrow_rs_subgrupo; ?></b> subgrupos de categorías</p>		
					<br> 
					<table class="table table-striped">
						<thead class="tabla_encabezado">
							<tr>
								<th><b>#</b></th>
								<th><b>Imagen</b></th>
								<th><b>Nombre</b></th>
								<th><b>Tipo de categoría</b></th>
								<th><b>Categorías vinculadas</b></th>
								<th><b>Categorías subgrupos</b></th>
							</tr>
						</thead>
						<tbody>
							<?php do { 
								$id_grupo_categoria = $row_rs_grupo_categoria['id_grupo_categoria'];
								$categoria_nombre = $row_rs_grupo_categoria['categoria_nombre'];
								$categoria_imagen = $row_rs_grupo_categoria['categoria_imagen'];
								$categoria_tipo = $row_rs_grupo_categoria['categoria_tipo'];

								$total_categorias_vinculadas = $array_grupo[$id_grupo_categoria];
								$total_categorias_subgrupo = $array_subgrupo[$id_grupo_categoria];
								if(!$total_categorias_vinculadas) {
									$total_categorias_vinculadas = 0;
								}
								if(!$total_categorias_subgrupo) {
									$total_categorias_subgrupo = 0;
								}

								$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
								$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
								if($categoria_imagen) {
									$imagen = $url_imagen.$categoria_imagen;
									$nombre_imagen = $categoria_imagen;
								}

								$super_class = null;
								if($total_categorias_vinculadas!=$total_categorias_subgrupo) {
									$super_class = 'categorias_con_subgrupos';
								}
								?>
								<tr class="<?php echo $super_class; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/08-ficha-categoria.php?categoria=<?php echo $id_grupo_categoria; ?>">
									<td><?php echo $id_grupo_categoria; ?></td>
									<td><img src="<?php echo $imagen; ?>"  height="40"></td>
									<td><?php echo $categoria_nombre; ?></td>
									<td><?php echo $categoria_tipo; ?></td>
									<td><strong><?php echo $total_categorias_vinculadas; ?></strong> categorías vinculadas</td>
									<td><strong><?php echo $total_categorias_subgrupo; ?></strong> categorías en subgrupos</td>

								</tr>		
								<?php } while($row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria)); ?>	          	
							</tbody>
						</table>				 

					</div>
				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
			<script type="text/javascript">
				$('tr[data-href]').on("click", function() {
					var redirigir = $(this).data('href');

					window.open(redirigir);
				});
			</script>
		</body>
		</html>