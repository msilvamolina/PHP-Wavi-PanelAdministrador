<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$grupo_categoria = trim($_GET['grupo_categoria']);
$subgrupo_categoria = trim($_GET['subgrupo_categoria']);
$provincia = trim($_GET['provincia']);
$ciudad = trim($_GET['ciudad']);

$regresar_a_promo = trim($_GET['regresar_a_promo']);


$pagina= $_GET['pagina'];
$pagina_get = $pagina;
if(!$pagina_get) {
	$pagina_get=1;
}
if($pagina) {
	$pagina = $pagina-1;
}
$limite = 50;
$arranca = $pagina*$limite;

if(!$grupo_categoria) {
	$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-general/07-grupos-categorias.php';
	header('location:'.$redirigir);
	exit;
}

conectar2('mywavi', 'WAVI');

//consultar en la base de datos
$query_rs_grupo_categoria = "SELECT id_grupo_categoria, categoria_nombre, categoria_imagen FROM grupo_categorias";
$rs_grupo_categoria = mysql_query($query_rs_grupo_categoria)or die(mysql_error());
$row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria);
$totalrow_rs_grupo_categoria = mysql_num_rows($rs_grupo_categoria);

do {
	$id_grupo_categoria = $row_rs_grupo_categoria['id_grupo_categoria'];
	$categoria_nombre = $row_rs_grupo_categoria['categoria_nombre'];
	$categoria_imagen = $row_rs_grupo_categoria['categoria_imagen'];

	$url_imagen = $Servidor_url."APLICACION/Imagenes/categorias/grandes/";

	$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
	if($categoria_imagen) {
		$imagen = $url_imagen.$categoria_imagen;
	}
	$array_categoria_nombre[$id_grupo_categoria] = $categoria_nombre;
	$array_categoria_imagen[$id_grupo_categoria] = $imagen;
} while($row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria));
//consultar en la base de datos
$query_rs_subgrupos = "SELECT id_subgrupo, subgrupo_nombre,id_grupo_dependiente FROM subgrupos_categorias  ORDER BY subgrupo_nombre ASC";
$rs_subgrupos = mysql_query($query_rs_subgrupos)or die(mysql_error());
$row_rs_subgrupos = mysql_fetch_assoc($rs_subgrupos);
$totalrow_rs_subgrupos = mysql_num_rows($rs_subgrupos);

do {
	$id_subgrupo = $row_rs_subgrupos['id_subgrupo'];
	$subgrupo_nombre = $row_rs_subgrupos['subgrupo_nombre'];
	$id_grupo_dependiente = $row_rs_subgrupos['id_grupo_dependiente'];

	$array_subgrupo_nombre[$id_subgrupo] = $subgrupo_nombre;

	$array_subgrupos[$id_grupo_dependiente] .= '-'.$id_subgrupo;

} while($row_rs_subgrupos = mysql_fetch_assoc($rs_subgrupos));

$WHERE = 'vinculacion_negocio_categorias.id_grupo_categoria = '.$grupo_categoria;

if($subgrupo_categoria) {
	$WHERE .=' AND vinculacion_negocio_categorias.id_subgrupo_categoria = '.$subgrupo_categoria;
}

if($provincia) {
	$WHERE .=' AND negocios.negocio_provincia = '.$provincia;
}

if($ciudad) {
	$WHERE .=' AND negocios.negocio_ciudad = '.$ciudad;
}
//consultar en la base de datos
$query_rs_vinculaciones = "SELECT vinculacion_negocio_categorias.id_vinculacion, vinculacion_negocio_categorias.id_negocio, vinculacion_negocio_categorias.id_grupo_categoria, vinculacion_negocio_categorias.id_subgrupo_categoria, negocios.negocio_nombre FROM vinculacion_negocio_categorias, negocios WHERE vinculacion_negocio_categorias.id_negocio = negocios.id_negocio AND $WHERE ORDER BY negocios.negocio_nombre ASC LIMIT $arranca,$limite ";
$rs_vinculaciones = mysql_query($query_rs_vinculaciones)or die(mysql_error());
$row_rs_vinculaciones = mysql_fetch_assoc($rs_vinculaciones);
$totalrow_rs_vinculaciones = mysql_num_rows($rs_vinculaciones);


//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);
do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];
	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));

//consultar en la base de datos
$query_rs_array_ciudades = "SELECT id_ciudad, ciudad_nombre, id_provincia  FROM ciudades ORDER BY ciudad_nombre ";
$rs_array_ciudades = mysql_query($query_rs_array_ciudades)or die(mysql_error());
$row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades);
$totalrow_rs_array_ciudades = mysql_num_rows($rs_array_ciudades);

do {
	$ciudad_provincia = $row_rs_array_ciudades['id_provincia'];
	$id_ciudad = $row_rs_array_ciudades['id_ciudad'];
	$ciudad_nombre = $row_rs_array_ciudades['ciudad_nombre'];
	$array_ciudades[$id_ciudad] = $ciudad_nombre;

	if(!$array_provincias_ciudades[$ciudad_provincia]) {
		$array_provincias_ciudades[$ciudad_provincia] = $id_ciudad;
	} else {
		$array_provincias_ciudades[$ciudad_provincia] .= '-'.$id_ciudad;
	}
	
} while($row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades));

//consultar en la base de datos
$query_rs_negocios = "SELECT id_bender_negocio, id_negocio, negocio_nombre,  negocio_provincia, negocio_ciudad, negocio_descripcion, negocio_palabras_claves, negocio_categoria, fecha_carga,usuario_que_carga FROM negocios ORDER BY id_negocio DESC";
$rs_negocios = mysql_query($query_rs_negocios)or die(mysql_error());
$row_rs_negocios = mysql_fetch_assoc($rs_negocios);
$totalrow_rs_negocios = mysql_num_rows($rs_negocios);


do {
	$id_bender_negocio = $row_rs_negocios['id_bender_negocio'];
	$id_negocio = $row_rs_negocios['id_negocio'];
	$negocio_nombre = $row_rs_negocios['negocio_nombre'];
	$negocio_provincia = $row_rs_negocios['negocio_provincia'];
	$negocio_ciudad = $row_rs_negocios['negocio_ciudad'];

	$fecha_carga = $row_rs_negocios['fecha_carga'];
	$usuario_que_carga = $row_rs_negocios['usuario_que_carga'];

	$array_bender_negocio[$id_negocio] = $id_bender_negocio;
	$array_bender_nombre[$id_negocio] = $negocio_nombre;
	$array_bender_provincia[$id_negocio] = $negocio_provincia;
	$array_bender_ciudad[$id_negocio] = $negocio_ciudad;

	$array_bender_fecha_carga[$id_negocio] = $fecha_carga;
	$array_bender_usuario[$id_negocio] = $usuario_que_carga;
} while($row_rs_negocios = mysql_fetch_assoc($rs_negocios));

//consultar en la base de datos
$query_rs_usuarios = "SELECT id_usuario, usuario_nombre, usuario_apellido FROM usuarios_cargadores ";
$rs_usuarios = mysql_query($query_rs_usuarios)or die(mysql_error());
$row_rs_usuarios = mysql_fetch_assoc($rs_usuarios);
$totalrow_rs_usuarios = mysql_num_rows($rs_usuarios);

do {
	$id_usuario = $row_rs_usuarios['id_usuario'];
	$usuario_nombre = $row_rs_usuarios['usuario_nombre'];
	$usuario_apellido = $row_rs_usuarios['usuario_apellido'];

	$array_usuarios[$id_usuario] = $usuario_nombre;
} while($row_rs_usuarios = mysql_fetch_assoc($rs_usuarios));

desconectar();
$array_subgrupos_elegido = $array_subgrupos[$grupo_categoria];

$explorar_array_subgrupo = explode('-', $array_subgrupos_elegido);
$url_imagen = $Servidor_url."APLICACION/Imagenes/categorias/grandes/";

$pagina_actual_variables = $Servidor_url_documento.'?regresar_a_promo='.$regresar_a_promo.'&grupo_categoria='.$grupo_categoria.'&subgrupo_categoria='.$subgrupo_categoria.'&provincia='.$provincia.'&ciudad='.$ciudad.'&';

$pagina_siguiente = $pagina+2;
$pagina_anterior = $pagina;
$disabled_siguiente = null;
$disabled_anterior = null;
$link_siguiente = $pagina_actual_variables.'pagina='.$pagina_siguiente;
$link_anterior = $pagina_actual_variables.'pagina='.$pagina_anterior;
if($pagina_anterior<=0) {
	$disabled_anterior = 'disabled';
	$link_anterior = null;
}

if($regresar_a_promo) {
	$link_negocio = $Servidor_url."PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/03-editar-promocion.php?promo=".$regresar_a_promo."&negocio=";
} else {
	$link_negocio = $Servidor_url."PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios-ficha.php?negocio=";
}


?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->

	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}

		.tabla {
			width: 100%;
		}
		.tabla tr td{
			padding: 10px;
		}	

		.tabla tr:nth-of-type(2n) {
			background: #f5e5f2;
		}
		.no_hay_imagen{
			color: #acacac;
		}
		.tabla_encabezado {
			color: red;
		}

		tr {
			cursor: pointer;
		}

		.categorias_con_subgrupos {
			background: #f90 !important;
			color: #fff !important;
		}

		.grupo_imagen {
			width: 80px;
			border-radius: 50%;
		}

		.usuario_avatar {
			width: 50px;
			border-radius: 50%;
		}
		td {
			cursor: pointer;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->
			<div class="cd-form floating-labels" style="max-width:1600px">
				<legend id="txt_nueva_categoria"><img src="<?php echo $array_categoria_imagen[$grupo_categoria]; ?>" class="grupo_imagen"><b><?php echo $array_categoria_nombre[$grupo_categoria]; ?></b></legend>
				<form method="get" action="<?php echo $Servidor_url_documento;?>" name="form">
					<input type="hidden" name="grupo_categoria" value="<?php echo $grupo_categoria; ?>" >
					<p class="cd-select">
						<select name="subgrupo_categoria" class="select_class" id="select_subgrupo_1" onchange="document.forms.form.submit()">
							<option value="0">Todos los subgrupos</option>	
							<?php foreach ($explorar_array_subgrupo as $id_subgrupo) {
								if($id_subgrupo) {
									$subgrupo_nombre = $array_subgrupo_nombre[$id_subgrupo];

									$selected = null;

									if($subgrupo_categoria==$id_subgrupo) {
										$selected = 'selected';
									}
									?>
									<option <?php echo $selected; ?> value="<?php echo $id_subgrupo; ?>"><?php echo $subgrupo_nombre; ?></option>	
									<?php } }?>
								</select></p>
							</form>
							<br><br>
							<h2><b>Provincia</b></h2>
							<form method="get" action="<?php echo $Servidor_url_documento;?>" name="form2">
								<input type="hidden" name="grupo_categoria" value="<?php echo $grupo_categoria; ?>" >
								<input type="hidden" name="subgrupo_categoria" value="<?php echo $subgrupo_categoria; ?>" >
								<input type="hidden" name="regresar_a_promo" value="<?php echo $regresar_a_promo; ?>" >
								<p class="cd-select">
									<select name="provincia" class="select_class" id="select_subgrupo_1" onchange="document.forms.form2.submit()">
										<option value="0">Todas las provincias</option>	

										<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) { 
											$selected = null;

											if($provincia==$id_provincia) {
												$selected = 'selected';
											}
											?>
											<option <?php echo $selected; ?> value="<?php echo $id_provincia; ?>"><?php echo $provincia_nombre; ?></option>	
											<?php } ?>

										</select></p>
									</form>
									<?php if($provincia) { ?>
									<form method="get" action="<?php echo $Servidor_url_documento;?>" name="form3">
										<input type="hidden" name="grupo_categoria" value="<?php echo $grupo_categoria; ?>" >
										<input type="hidden" name="subgrupo_categoria" value="<?php echo $subgrupo_categoria; ?>" >
										<input type="hidden" name="provincia" value="<?php echo $provincia; ?>" >

										<p class="cd-select">
											<select name="ciudad" class="select_class" id="select_subgrupo_1" onchange="document.forms.form3.submit()">
												<option value="0">Todas las ciudades</option>	

												<?php 
												$explorar_ciudades = explode('-', $array_provincias_ciudades[$provincia]);

												foreach ($explorar_ciudades as $id_ciudad) { 
													$selected = null;

													if($ciudad==$id_ciudad) {
														$selected = 'selected';
													}
													?>
													<option <?php echo $selected; ?> value="<?php echo $id_ciudad; ?>"><?php echo $array_ciudades[$id_ciudad]; ?></option>	
													<?php } ?>

												</select></p>
											</form>	
											<?php } ?>
											<nav role="navigation">
												<ul class="cd-pagination">
													<li class="button"><a class="<?php echo $disabled_anterior; ?>" href="<?php echo $link_anterior; ?>">Anterior</a></li>
													<li class="button"><a  class="<?php echo $disabled_siguiente; ?>"  href="<?php echo $link_siguiente; ?>">Siguiente</a></li>
												</ul>
											</nav> <!-- cd-pagination-wrapper -->		
											<?php if($totalrow_rs_vinculaciones) { ?>	
											<table class="table table-striped">
												<thead class="tabla_encabezado">
													<tr>
														<th><b>#</b></th>
														<th><b>Negocio</b></th>
														<th style="text-align:center"><b>Categoría</b></th>
														<th><b>Ubicación</b></th>
														<th><b>Usuario</b></th>
														<th><b>Fecha Carga</b></th>

													</tr>
												</thead>
												<tbody>
													<?php  $imagen_bender = $Servidor_url.'PANELADMINISTRADOR/img/usuarios_cargadores/bender-circulo.png';

													do {
														$id_vinculacion = $row_rs_vinculaciones['id_vinculacion'];
														$id_negocio = $row_rs_vinculaciones['id_negocio'];
														$id_grupo_categoria = $row_rs_vinculaciones['id_grupo_categoria'];
														$id_subgrupo_categoria = $row_rs_vinculaciones['id_subgrupo_categoria'];

														$id_bender_negocio = $array_bender_negocio[$id_negocio];
														$negocio_nombre = $array_bender_nombre[$id_negocio];

														$imagen = $array_categoria_imagen[$id_grupo_categoria];
														$grupo_nombre = $array_categoria_nombre[$id_grupo_categoria];
														$subgrupo_nombre = $array_subgrupo_nombre[$id_subgrupo_categoria];

														$negocio_ciudad = $array_bender_ciudad[$id_negocio];
														$negocio_provincia = $array_bender_provincia[$id_negocio];

														$negocio_ciudad = $array_ciudades[$negocio_ciudad];
														$negocio_provincia = $array_provincias[$negocio_provincia];

														$usuario_que_carga = $array_bender_usuario[$id_negocio];

														$fecha_carga = $array_bender_fecha_carga[$id_negocio];
														$fecha_carga = cuantoHace($fecha_carga);
														$usuario_nombre_txt = $array_usuarios[$usuario_que_carga];
														$variable_grupo_nombre = null;
														$variable_grupo_nombre .= '<img src="'.$imagen.'" class="usuario_avatar"/><br>';
														$variable_grupo_nombre .= $grupo_nombre;
														$variable_grupo_nombre .= '<br><b>'.$subgrupo_nombre.'</b>';
														?>
														<tr class="<?php echo $super_class; ?>" data-href="<?php echo $link_negocio.$id_negocio; ?>">
															<td><b style="color:red"><?php echo $id_negocio; ?></b></td>
															<td width="20%"><?php echo $negocio_nombre; ?></td>
															<td style="text-align:center"><?php echo $variable_grupo_nombre; ?></td>
															<td><strong><?php echo $negocio_ciudad; ?></strong>,<br><?php echo $negocio_provincia; ?></td>
															<td>
																<?php if($id_bender_negocio) { ?>
																<img class="usuario_avatar" src="<?php echo $imagen_bender; ?>">
																<?php 
																$usuario_nombre_txt = 'Bender y '.$usuario_nombre_txt;
															} ?>
															<img class="usuario_avatar" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/usuarios_cargadores/<?php echo $usuario_que_carga; ?>.jpg">
															<br><b><?php echo $usuario_nombre_txt; ?></b></td>
															<td><strong><?php echo $fecha_carga; ?></strong></td>
														</tr>
														<?php } while($row_rs_vinculaciones = mysql_fetch_assoc($rs_vinculaciones)); ?>
													</tbody>
												</table>		
												<?php } else { ?>
												<center>No se encontraron negocios</center>
												<?php } ?>        
												<nav role="navigation">
													<ul class="cd-pagination">
														<li class="button"><a class="<?php echo $disabled_anterior; ?>" href="<?php echo $link_anterior; ?>">Anterior</a></li>
														<li class="button"><a  class="<?php echo $disabled_siguiente; ?>"  href="<?php echo $link_siguiente; ?>">Siguiente</a></li>
													</ul>
												</nav> <!-- cd-pagination-wrapper -->	  
											</div>
										</div> <!-- .content-wrapper -->
									</main> 
									<?php include('../../includes/pie-general.php');?>
									<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
									<script type="text/javascript">
										$('tr[data-href]').on("click", function() {
											var link = $(this).data('href');
											window.location.href = link;
											//window.open();
										});
									</script>
								</body>
								</html>