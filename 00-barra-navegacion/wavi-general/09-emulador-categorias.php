<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$nombre_calle ="Virgen de la merced 706, San Miguel de Tucumán, Tucumán";
$latitud = "-26.8218435";
$longitud = "-65.1995455";
?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBcvJODUo2VKA-QuRMAEu8EwYtSNg1cJV0'></script>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/cargar-negocio-google-map.css"> 

	<style type="text/css">
		.cd-main-content .content-wrapper {
			padding: 0 !important;
		}
		#pantalla_emulador {
			max-width:347px; 
			margin:0 auto;
			background: url(<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/cel-samsung2.png);
			height: 700px;
			background-repeat: no-repeat; 
		}
		iframe {
			margin-top: 73px;
			margin-left: 15px;
			width: 317px;
			height: 558px;
		}
		.contenedor_general {
			padding-top: 65px;
		}

		.celular {
			max-width:600px; margin:0 auto;
		}

		.mapa {
			max-width:1200px; margin:0 auto;
			display: none;

		}

		.contenedor_link {
			padding: 40px;
		}

		h3 {
			padding: 40px;
			text-align: center;
			width: 100%;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->

			<div class="contenedor_general" >
				<section class="celular">
					<div id="pantalla_emulador" >
						<iframe src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/emulador/categorias.php"></iframe>
					</div>			
					<h3 id="establecer_ubicacion"><a onclick="mostrar_mapa()">Establecer Ubicación</a></h3>

				</section>
				<section class="mapa">
					<center>
						<div class="contenedor_link">
						<div id="datos"></div>
							<?php include('includes/mapa.php'); ?>
						</div>
					</center>
				</section>		

			</div>
		</div> <!-- .content-wrapper -->
	</main> 
	<?php include('../../includes/pie-general.php');?>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
	<script type="text/javascript">

		var infowindow = new google.maps.InfoWindow({});

		jQuery(document).ready(function(){
	     //obtenemos los valores en caso de tenerlos en un formulario ya guardado en la base de datos
	     //Inicializamos la función de google maps una vez el DOM este cargado
	     initialize(); 
	 });
	   	//si no hay información guardada, ejecuta automaticamente los botones con la direccion que ingreso anteriormente

	function mostrar_mapa () {
		$('.mapa').show();
		$('#establecer_ubicacion').html('<a onclick="ocultar_mapa()">Ocultar Mapa</a>');
		
		initialize();
	}
		function ocultar_mapa () {
		$('.mapa').hide();
		$('#establecer_ubicacion').html('<a onclick="mostrar_mapa()">Establecer Ubicación</a>');
		
		initialize();
	}
	   	function initialize() {
	   		geocoder = new google.maps.Geocoder();

	   		$lat = <?php echo $latitud; ?>;
	   		$long = <?php echo $longitud; ?>;
	   		$zoom = 18;

	   		var latlng = new google.maps.LatLng(<?php echo $latitud; ?>, <?php echo $longitud; ?>);
	   		var myOptions = {
	   			zoom: 18,
	   			center: latlng,
	   			mapTypeId: google.maps.MapTypeId.ROADMAP
	   		}

	   		map= new google.maps.Map(document.getElementById("map_canvas"), myOptions);

	   		marker = new google.maps.Marker({
	   			position: latlng,
	   			map: map,
	   			draggable: true
	   		});

	   		updatePosition(latlng);
	   		google.maps.event.addListener(marker, 'dragend', function(){
	   			codeAddress2(marker.getPosition());
	   		});  
	   	}


//funcion que traduce la direccion en coordenadas
function codeAddress() {
	var address = document.getElementById("direccion").value;
	$.ajax({
		url: "https://maps.googleapis.com/maps/api/geocode/json?sensor=true&address="+address,
		dataType: "json",
		success: function (data) {
			response = JSON.stringify(data);
			var json = jQuery.parseJSON(response);

			mapa = map;
			marcador = marker;

			if(json.status=='OK') {
				var direccion = json.results[0].formatted_address;
				var datos = json.results[0].geometry.location;

				mapa.setCenter(datos);
				mapa.setZoom(17);  
				marcador.setPosition(datos);    
		        //coloco el marcador en dichas coordenadas
		        //actualizo el formulario      
		        updatePosition(marcador.getPosition(), direccion);
		    }
		}
	});
}

function codeAddress2(latLng) {
	var latitud_longitud = latLng.lat()+","+latLng.lng();
	$.ajax({
		url: "https://maps.googleapis.com/maps/api/geocode/json?sensor=true&latlng="+latitud_longitud,
		dataType: "json",
		success: function (data) {
			response = JSON.stringify(data);
			var json = jQuery.parseJSON(response);

			mapa = map;
			marcador = marker;

			if(json.status=='OK') {
				var direccion = json.results[0].formatted_address;
				jQuery('#direccion').val(direccion);
				updatePosition(marker.getPosition());
		    }
		}
	});
}

function updatePosition(latLng)
{
	jQuery('#lat').val(latLng.lat());
	jQuery('#long').val(latLng.lng());

	jQuery('#lat_txt').html(latLng.lat());
	jQuery('#long_txt').html(latLng.lng());

	establecer_conexion(latLng);
}

function establecer_conexion(latLng) {
	var address = document.getElementById("direccion").value;

	var sendInfo = {
		direccion: address,
		latitud: latLng.lat(),
		longitud: latLng.lng()
	};

	$.ajax({
		type: "POST",
		url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/ajax/establecer-ubicacion.php",
		dataType: "json",
		success: function (data) {
			response = JSON.stringify(data);
			var json = jQuery.parseJSON(response);
			if(json.respuesta != "OK") {
				alert(json.respuesta);
			}
		},

		data: sendInfo
	});
}
</script>
</body>
</html>