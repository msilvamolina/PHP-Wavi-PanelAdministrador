<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

conectar2('mywavi', 'sitioweb');
//consultar en la base de datos
$query_rs_diarios = "SELECT id_diario, diario_nombre, diario_imagen, id_provincia, seccion FROM diarios ORDER BY diario_nombre ASC ";
$rs_diarios = mysql_query($query_rs_diarios)or die(mysql_error());
$row_rs_diarios = mysql_fetch_assoc($rs_diarios);
$totalrow_rs_diarios = mysql_num_rows($rs_diarios);

desconectar();

conectar2('mywavi', 'WAVI');
//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre  FROM provincias ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);

do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];

	$array_provincias[335] = "Todas las provincias";
	$array_provincias[$id_provincia] = $provincia_nombre;
} while ($row_rs_provincias = mysql_fetch_assoc($rs_provincias));


desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<style type="text/css">
		.btn_eliminar {
			text-align: right;
			width: 100%;
		}

		a {
			cursor: pointer;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="contenedor">
				<div >					<!-- Contenido de la Pagina-->	
					<div class="cd-form floating-labels">
						<section id="crear_categoria" >							
							<fieldset >
								<form onsubmit="return validar_formulario()" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/00-cargar-nota-db.php" method="POST">
									<input  type="hidden" id="elementos_contador" value="2">
									<input type="hidden" name="diario" id="diario" value="0" >

									<legend id="txt_nueva_categoria">Nueva Nota</legend>

									<div id="demoBasic"  ></div>		

									<div class="icon">
										<label class="cd-label" for="cd-company">Titulo</label>
										<input class="company" type="text" name="titulo" id="nueva_categoria_nombre" required>
									</div> 			    

									<div class="alinear_centro">
										<input type="submit" value="Continuar" id="btn_nueva_categoria">
									</div>
								</form>
							</fieldset>	
						</section>    	

					</div>
				</div>
			</div> <!-- .content-wrapper -->
		</main> 
		<?php include('../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
		<script type="text/javascript">
			var ddData = [
			<?php
			$url_imagen = $Servidor_url.'APLICACION/Imagenes/diarios/';
			$i = 1;
			do { 
				$id_diario = $row_rs_diarios['id_diario'];
				$seccion = $row_rs_diarios['seccion'];


				$diario_nombre = $row_rs_diarios['diario_nombre'];
				$diario_imagen = $row_rs_diarios['diario_imagen'];
				$id_provincia = $row_rs_diarios['id_provincia'];


				if($diario_imagen) {
					$imagen = $url_imagen.$diario_imagen;
				} else {
					$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
				}

				$descripcion = $array_provincias[$id_provincia];
				?>
				{
					text: "<?php echo $diario_nombre; ?>",
					value: <?php echo $id_diario; ?>,
					description: "<?php echo $descripcion; ?>",
					imageSrc: "<?php echo $imagen; ?>"
					<?php if($i == $totalrow_rs_diarios) {
						echo '}';
					} else {
						echo '},';
					}

					$i++; } while($row_rs_diarios = mysql_fetch_assoc($rs_diarios)); ?>
					];

//Dropdown plugin data


$('#demoBasic').ddslick({
	data: ddData,
	width: 600,
	imagePosition: "left",
	selectText: "Elegí un diario",
	onSelected: function (data) {
		var valor = data.selectedData.value;
		document.getElementById("diario").value = valor;
	}
});	

function validar_formulario() {
	var diario = document.getElementById("diario").value;

	if(diario==0) {
		alert('Tenés que elegir un diario');
		return false;
	} else {
		return true;
	}

}
</script>
</body>
</html>