<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

	$promo = $_GET['promo'];

	$id_administrador = trim($_GET['id_administrador']);

	$redireccionar_dp = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/03-editar-promocion.php?promo='.$promo.'&imagen_cargada=1';

?>
<!DOCTYPE html>
<html>
<head>
<html lang="es" class="no-js">
<head>
	<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="dropzone/dropzone.css"> <!-- Resource style -->
<link rel="stylesheet" href="dropzone/dropzone2.css"> <!-- Resource style -->
<script src="dropzone/dropzone.js"></script>

</head>
<body>
<center>
	<div id="imagen_cargando" style="display:none;margin-top:40px">
			<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/loader.gif">
	</div>
</center>
<div  id="contenedor_drag_drop">
		<div id="dropzone"><form action="php/01-cargar-imagen-promo-db.php" class="dropzone needsclick" id="myAwesomeDropzone">
	  <input type="hidden" name="promo" value="<?php echo $promo; ?>">
	  <input type="hidden" name="id_administrador" value="<?php echo $id_administrador; ?>">

	  <div class="fallback" style="display:none">
    <input name="file" type="file" multiple />
  </div>
	  <div class="dz-message needsclick">
	  	<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/icono-drag-drop.png" class="icono_drag_drop">
	  	<br>
	    Arrastrá la imagen a este recuadro para subirla.
	  </div>
</form></div>
<script src="jquery/jquery-2.1.4.js"></script>

<script type="text/javascript">
	Dropzone.options.myAwesomeDropzone = {
  init: function() {
    this.on("complete", function(file) {
     	imagen_cargada();
     });
  },
	accept: function(file, done) {
	    if ((file.type != "image/jpeg") && (file.type != "image/png")){
	        alert("Error! Solo podés cargar imágenes JPG y PNG");
	        this.removeAllFiles();
	    } else { done(); }
	}
};

function imagen_cargada() {
	$('#contenedor_drag_drop').hide();
	$('#imagen_cargando').show();
	$.ajax({
		url: "02-mostrar-imagen-promo.php?promo=<?php echo $promo;?>",
		success: function (resultado) {
			$('#imagen_cargando').html(resultado);
			//refrescamos para que se borren los temporales
		    setInterval( function() {
				borrar_temporales();	
			}, 1500 );
		}
	});	
}

function borrar_temporales() {
	$.ajax({
		url: "php/02-borrar-temporales-promo.php?promo=<?php echo $promo;?>",
		success: function (resultado) {
			$('#imagen_cargando').html(resultado);
			parent.opener.location = '<?php echo $redireccionar_dp;?>';
			self.close();
		}
	});	
}

</script>
</body>
</html>