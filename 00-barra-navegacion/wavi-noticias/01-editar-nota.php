<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$noticia = trim($_GET['noticia']);
$imagen_cargada = trim($_GET['imagen_cargada']);

if(!$noticia) {
	$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/00-cargar-nota.php';
	header('location:'.$redirigir);
	exit;
}

conectar2('mywavi', 'sitioweb');

$query_rs_imagen = "SELECT id_foto, nombre_foto, fecha_carga, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_publicacion = $noticia ORDER BY id_foto DESC ";
$rs_imagen = mysql_query($query_rs_imagen)or die(mysql_error());
$row_rs_imagen = mysql_fetch_assoc($rs_imagen);
$totalrow_rs_imagen = mysql_num_rows($rs_imagen);

if($imagen_cargada) {
	$id_foto = $row_rs_imagen['id_foto'];
	$redireccionar = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/03-recortar-foto.php?foto='.$id_foto.'&noticia='.$noticia;
	header('location:'.$redireccionar);
	exit;
}

//consultar en la base de datos
$query_rs_diarios = "SELECT id_diario, diario_nombre, diario_imagen, id_provincia FROM diarios ORDER BY diario_nombre ASC ";
$rs_diarios = mysql_query($query_rs_diarios)or die(mysql_error());
$row_rs_diarios = mysql_fetch_assoc($rs_diarios);
$totalrow_rs_diarios = mysql_num_rows($rs_diarios);

//consultar en la base de datos
$query_rs_noticias = "SELECT id_diario, noticia_titulo, noticia_bajada, fecha_noticia, noticia_palabras_claves FROM noticias WHERE id_noticia = $noticia";
$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
$totalrow_rs_noticias = mysql_num_rows($rs_noticias);

$noticia_id_diario = $row_rs_noticias['id_diario'];
$noticia_titulo = $row_rs_noticias['noticia_titulo'];
$noticia_bajada = $row_rs_noticias['noticia_bajada'];
$noticia_palabras_claves = $row_rs_noticias['noticia_palabras_claves'];
$fecha_noticia = $row_rs_noticias['fecha_noticia'];
$fecha_noticia_value = $fecha_noticia;

$check_atemporal = NULL;
$fecha_noticia_style = NULL;

if($fecha_noticia=='0000-00-00') {
	$check_atemporal = 'checked';
	$fecha_noticia_style = 'display:none';
	$fecha_noticia_value = date('Y-m-d');
}
//consultar en la base de datos
$query_rs_cuerpo = "SELECT id_cuerpo, orden, tamano_texto, cuerpo_tipo, contenido FROM noticias_cuerpo WHERE id_noticia = $noticia ORDER BY orden ASC ";
$rs_cuerpo = mysql_query($query_rs_cuerpo)or die(mysql_error());
$row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo);
$totalrow_rs_cuerpo = mysql_num_rows($rs_cuerpo);

//consultar en la base de datos
$query_rs_imagenes = "SELECT id_foto, recorte_foto_nombre FROM fotos_publicaciones WHERE id_publicacion = $noticia ";
$rs_imagenes = mysql_query($query_rs_imagenes)or die(mysql_error());
$row_rs_imagenes = mysql_fetch_assoc($rs_imagenes);
$totalrow_rs_imagenes = mysql_num_rows($rs_imagenes);

do {
	$id_foto = $row_rs_imagenes['id_foto'];
	$recorte_foto_nombre = $row_rs_imagenes['recorte_foto_nombre'];

	$array_imagenes[$id_foto] = $recorte_foto_nombre;
} while($row_rs_imagenes = mysql_fetch_assoc($rs_imagenes));


//consultar en la base de datos
$query_rs_direcciones = "SELECT id_direccion, direccion_nombre, direccion_numero, direccion_piso, direccion_ciudad, direccion_provincia  FROM direcciones_noticias WHERE id_noticia = $noticia ";
$rs_direcciones = mysql_query($query_rs_direcciones)or die(mysql_error());
$row_rs_direcciones = mysql_fetch_assoc($rs_direcciones);
$totalrow_rs_direcciones = mysql_num_rows($rs_direcciones);

desconectar();

conectar2('mywavi', 'WAVI');
//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre  FROM provincias ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);

do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];

	$array_provincias[335] = "Todas las provincias";
	$array_provincias[$id_provincia] = $provincia_nombre;
} while ($row_rs_provincias = mysql_fetch_assoc($rs_provincias));

//consultar en la base de datos
$query_rs_ciudad = "SELECT id_ciudad, ciudad_nombre, id_provincia FROM ciudades ORDER BY ciudad_nombre ASC ";
$rs_ciudad = mysql_query($query_rs_ciudad)or die(mysql_error());
$row_rs_ciudad = mysql_fetch_assoc($rs_ciudad);
$totalrow_rs_ciudad = mysql_num_rows($rs_ciudad);
do {
	$id_ciudad = $row_rs_ciudad['id_ciudad'];
	$ciudad_nombre = $row_rs_ciudad['ciudad_nombre'];
	$id_provincia = $row_rs_ciudad['id_provincia'];
	$array_ciudades[$id_ciudad] = $ciudad_nombre;
	if(!$array_ciudades_provincias[$id_provincia]) {
		$array_ciudades_provincias[$id_provincia] = $id_ciudad;
	} else {
		$array_ciudades_provincias[$id_provincia] = $array_ciudades_provincias[$id_provincia].'-'.$id_ciudad;
	}	
} while($row_rs_ciudad = mysql_fetch_assoc($rs_ciudad));

desconectar();

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form3.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<script src="<?php echo $Servidor_url;?>sistema/js/nicEdit-latest.js" type="text/javascript"></script>

</head>
<style type="text/css">
	.btn_eliminar {
		text-align: right;
		width: 100%;
	}

	a {
		cursor: pointer;
	}
	.boton_verde a{
		background: #48b617;
		color: #fff;
	}
	.boton_verde a:hover {
		background: #235d09 !important;
		color: #f6ff05;
	}	
	.boton_rojo a{
		background: #c40000;
		color: #fff;
	}
	.boton_rojo a:hover {
		background: #9e0101 !important;
		color: #f6ff05;
	}
	h3 {
		margin-bottom: 5px;
		font-weight: bold;
	}

	.portada {
		color: #2E7D32;
		font-weight: bold;
	}

	.rojo {
		color: #F44336;
		font-weight: bold;
	}

	.verde {
		color: #2E7D32;
		font-weight: bold;
	}
	a {
		cursor: pointer;
	}
	.input_video {
		width: 100%;
		padding: 10px;
		margin-top: 15px;
	}

	.input_texto {
		width: 100%;
		height: 200px;
		padding: 10px;
		margin-top: 15px;
	}


	.video-container {
		position: relative;
		padding-bottom: 56.25%;
		padding-top: 30px; height: 0; overflow: hidden;
	}
	
	.video-container iframe,
	.video-container object,
	.video-container embed {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
	.video_youtube {
		width: 100%;
	}

	#formTexto {
		margin-top: -150px;
		padding: 20px;
	}	

	.img_delete {
		width: 30px;
	}

	.td_delete {
		padding: 10px;
		text-align: right;
		width: 30px;
	}
	.td_delete img {
		width: 30px;
		display: block;
	}
	.txt_categoria_elegida {
		padding: 10px;
		color: #2c97de;
		display: block;
	}
	.grupo_categoria {
		margin-left: -20px!important;
	}

	#section_categoria {
		background: #a7a7a7;
		padding: 30px;
		color: #fff;
	}

	#section_categoria h3 {
		font-size: 24px;
	}
	.select_class {
		background: #eeeeee !important;
	}
	.delete_categoria {
		float: right;
	}
	.delete_categoria img{
		width: 35px;
		margin-top: -5px;
	}
	.boton_amarillo {
		background: #f90;
	}
	.boton_amarillo:hover {
		background: red;
		color: #fff;
	}
</style>


</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="cd-popup" id="popup_agregar" role="alert">
				<div class="cd-popup-container">
					<p>¿Qué elemento querés agregar?</p>
					
					<div style="padding:10px">
						<a class="vc_btn_largo vc_btn_verde vc_btn_3d" onclick="agregar_texto()">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-plus fa-stack-1x fa-inverse"></i>
							</span>
							<b>Agregar Texto</b>
						</a>
						
						<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" onclick="agregar_imagen()">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-camera fa-stack-1x fa-inverse"></i>
							</span>
							<b>Agregar Imagen</b>
						</a>
						<a class="vc_btn_largo vc_btn_rojo vc_btn_3d" onclick="agregar_video()">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-youtube-play fa-stack-1x fa-inverse"></i>
							</span>
							<b>Agregar Video de youtube</b>
						</a>
					</div>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->
			<div class="cd-popup" id="popup_video" role="alert">
				<div class="cd-popup-container">
					<p>Copiá y pegá acá el link de tu video<br>
						<input class="input_video" type="text" id="input_video" placeholder="Ej: https://www.youtube.com/watch?v=yH2HIPOb-cE" /></p>
						<ul class="cd-buttons">
							<li id="btn_confirmar_noticia"><a onclick="confirmar_video_youtube()">Continuar</a></li>
							<li><a onclick="cerrar_popup()">Cancelar</a></li>
						</ul>
						<a href="#0" class="cd-popup-close img-replace"></a>
					</div> <!-- cd-popup-container -->
				</div> <!-- cd-popup -->

				<div class="cd-popup" id="popup_texto" role="alert">
					<div class="cd-popup-container">
						<p>Copiá o escribí el texto acá<br>
							<form method="POST" id="formTexto" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/08-agregar-texto.php" >
								<input type="hidden" name="noticia" value="<?php echo $noticia; ?>" /></p>
								<textarea class="input_texto" required name="texto" id="texto" ></textarea>
							</form>
							<ul class="cd-buttons">
								<li id="btn_confirmar_noticia"><a onclick="confirmar_agregar_video()">Continuar</a></li>
								<li><a onclick="cerrar_popup()">Cancelar</a></li>
							</ul>
							<a href="#0" class="cd-popup-close img-replace"></a>
						</div> <!-- cd-popup-container -->
					</div> <!-- cd-popup -->

					<div class="cd-popup" id="popup_borrar_elemento" role="alert">
						<div class="cd-popup-container">
							<p>¿Seguro querés borrar este elemento?<br>
								<ul class="cd-buttons">
									<li id="btn_confirmar_borrado_elemento"><a>Si</a></li>
									<li><a onclick="cerrar_popup()">Cancelar</a></li>
								</ul>
								<a href="#0" class="cd-popup-close img-replace"></a>
							</div> <!-- cd-popup-container -->
						</div> <!-- cd-popup -->
						<div class="cd-popup" id="popup_direccion" role="alert">
							<div class="cd-popup-container">
								<p>¿Estás seguro de querer borrar esta dirección?</p>
								<ul class="cd-buttons">
									<li id="btn_confirmar_direccion"></li>
									<li><a onclick="cerrar_popup()">No</a></li>
								</ul>
								<a href="#0" class="cd-popup-close img-replace"></a>
							</div> <!-- cd-popup-container -->
						</div> <!-- cd-popup -->
						<div class="contenedor">

							<div >					<!-- Contenido de la Pagina-->	

								<div class="cd-form floating-labels">
									<section id="crear_categoria" >							
										<fieldset >
											<form onsubmit="return validar_formulario()" id="myForm" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/01-editar-nota-db.php" method="POST">
												<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/02-ficha-nota.php?noticia=<?php echo $noticia;?>" class="vc_btn_largo vc_btn_rojo vc_btn_3d" style="width:250px;float:right">
													<span class="fa-stack fa-lg pull-left">
														<i class="fa fa-circle fa-stack-2x"></i>
														<i class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
													</span>
													<b>Ficha de la noticia</b>
												</a>
												<legend id="txt_nueva_categoria">Editar Nota</legend>

												<div id="demoBasic"  ></div>		

												<br>
												<div>
													<h4>Sección <a href="#">[+ Agregar]</a></h4>
													<p class="cd-select icon">
														<select class="budget" id="select_elegir_provincia" onchange="ciudades_dependientes(this.value)">
															<option value="0" >Elegí una provincia</option>
															<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
																$provincia_elegida=null;
																if($id_provincia==25) {
																	$provincia_elegida="selected=";
																}
																echo '<option value="'.$id_provincia.'" '.$provincia_elegida.'>'.$provincia_nombre.'</option>';
															}?>
														</select>
													</p>
												</div>
												<div>
													<h4>Grupo <a href="#">[+ Agregar]</a></h4>
													<p class="cd-select icon">
														<select class="budget" id="select_elegir_provincia" onchange="ciudades_dependientes(this.value)">
															<option value="0" >Elegí una provincia</option>
															<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
																$provincia_elegida=null;
																if($id_provincia==25) {
																	$provincia_elegida="selected=";
																}
																echo '<option value="'.$id_provincia.'" '.$provincia_elegida.'>'.$provincia_nombre.'</option>';
															}?>
														</select>
													</p>
												</div>
												<div class="icon">
													<label class="cd-label" for="cd-company">Cartucho</label>
													<input class="company" type="text" name="cartucho" value="<?php echo $noticia_cartucho; ?>" id="titulo" required>
												</div> 			    
												<div class="icon">
													<label class="cd-label" for="cd-company">Titulo</label>
													<input class="company" type="text" name="titulo" value="<?php echo $noticia_titulo; ?>" id="titulo" required>
												</div> 			    
												<div class="icon">
													<label class="cd-label" for="cd-textarea">Bajada</label>
													<textarea class="message" required name="bajada" id="bajada" ><?php echo $noticia_bajada; ?></textarea>
												</div>
												<div class="icon">
													<label class="cd-label" for="cd-company">Palabras claves</label>
													<input class="company" type="text" value="<?php echo $noticia_palabras_claves; ?>" name="palabras_claves" id="palabras_claves" required>
												</div> 	
												<ul class="cd-form-list">
													<li>
														<input onchange="nota_atemporal()" type="checkbox" name="check_atemporal" <?php echo $check_atemporal; ?> id="check_atemporal" value="1">
														<label for="cd-checkbox-1">Nota atemporal</label>
													</li>
												</ul>								    
												<div class="icon" id="fecha_noticia" style="<?php echo $fecha_noticia_style; ?>">
													<label class="cd-label" for="cd-company">Fecha noticia</label>
													<input class="company" type="date" value="<?php echo $fecha_noticia_value;?>" step="1" name="fecha_noticia" id="fecha_noticia" required>
												</div>

												<input  type="hidden" id="elementos_contador" value="1">
												<input type="hidden" name="diario" id="diario" value="0" >
												<input  type="hidden" id="noticia" name="noticia" value="<?php echo $noticia; ?>">
												<br>
												<legend id="txt_nueva_categoria">Direcciones</legend>
												<div id="direcciones">	
													<?php 
													$i = 1;
													if($totalrow_rs_direcciones) {
														do {
															$id_direccion = $row_rs_direcciones['id_direccion'];
															$direccion_nombre = $row_rs_direcciones['direccion_nombre'];
															$direccion_numero = $row_rs_direcciones['direccion_numero'];
															$direccion_piso = $row_rs_direcciones['direccion_piso'];
															$direccion_ciudad = $row_rs_direcciones['direccion_ciudad'];
															$direccion_provincia = $row_rs_direcciones['direccion_provincia'];

															$direccion_nombre = $direccion_nombre.' '.$direccion_numero.' '.$direccion_piso;
															$direccion_nombre = trim($direccion_nombre);
															if(!$id_direccion) {
																$id_direccion = 0;
															}
															?>	
															<div id="direccion_agregada_<?php echo $i; ?>">
																<table width="100%" >
																	<tr>
																		<td >
																			<p class="cd-select icon">
																				<select name="select_provincia[<?php echo $i;?>]" class="budget"  onchange="cargar_ciudades(this.value,<?php echo $i; ?>)">									
																					<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
																						$elegido = null;
																						if($id_provincia==$direccion_provincia) {
																							$elegido = 'selected';
																						}
																						if($id_provincia) {
																							echo '<option value="'.$id_provincia.'" '.$elegido.'>'.$provincia_nombre.'</option>';
																						}
																					}
																					?>
																				</select></p>						    	
																			</td>
																			<td >
																				<p class="cd-select icon">
																					<select name="select_ciudad[<?php echo $i;?>]" class="budget" id="select_ciudad_<?php echo $i; ?>" >									
																						<?php 
																						$explorar_ciudades = explode('-', $array_ciudades_provincias[$direccion_provincia]);
																						foreach ($explorar_ciudades as $id_ciudad) {
																							$elegido = null;
																							$ciudad_nombre = $array_ciudades[$id_ciudad];
																							if($id_ciudad==$direccion_ciudad) {
																								$elegido = 'selected';
																							}
																							if($id_ciudad) {
																								echo '<option value="'.$id_ciudad.'" '.$elegido.'>'.$ciudad_nombre.'</option>';
																							}
																						}
																						?>
																					</select></p>					    	
																				</td>		
																				<td class="td_delete"><a onclick="eliminar_direccion(<?php echo $i; ?>, <?php echo $id_direccion; ?>)"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a></td>					    				    		
																			</tr>
																		</table>					
																		<table width="100%" >
																			<tr>
																				<td width="60%">
																					<input  type="hidden" name="direccion_id[<?php echo $i;?>]" value="<?php echo $id_direccion; ?>"  >							
																					<input  type="text" name="direccion_nombre[<?php echo $i;?>]" value="<?php echo $direccion_nombre; ?>" required placeholder="Dirección" >							
																					
																				</td>						    				    		
																			</tr>
																			<tr><td>&nbsp;</td></tr>
																		</table>
																	</div>
																	<?php $i++; } while($row_rs_direcciones = mysql_fetch_assoc($rs_direcciones)); }?>
																	
																</div>
																<input  type="hidden" id="direcciones_contador" value='<?php echo $i; ?>'>
																<input  type="hidden" id="borrar_direccion" value='0'>
																<input  type="hidden" id="borrar_direccion" value='1'>
																<div class="alinear_centro">
																	<a class="boton_azul boton_amarillo" onclick="agregar_direccion()" id="btn_agregar_direccion">[+] Agregar dirección</a>
																</div>
																<br><br><br>
																<div id="cuerpo">
																	<h3>Cuerpo:</h3>
																	<table class="table table-striped">
																		<tbody>
																			<?php
																			$ruta_img = $Servidor_url.'APLICACION/Imagenes/notas/recortes/';
																			$i=1;
																			do {
																				$id_cuerpo = $row_rs_cuerpo['id_cuerpo'];
																				$cuerpo_tipo = $row_rs_cuerpo['cuerpo_tipo'];
																				$contenido = $row_rs_cuerpo['contenido'];
																				$tamano_texto = $row_rs_cuerpo['tamano_texto'];

																				$orden = $row_rs_cuerpo['orden'];

																				if($cuerpo_tipo=="imagen") {
																					$imagen = $ruta_img.$array_imagenes[$contenido];
																					?>
																					<input type="hidden" name="elemento[<?php echo $id_cuerpo;?>]" value="<?php echo $contenido; ?>" >
																					<tr>
																						<td width="60"><input type="text" name="orden[<?php echo $id_cuerpo;?>]"  maxlength="2" value="<?php echo $i; ?>"></td>
																						<td><img src="<?php echo $imagen; ?>" width="100%"></td>
																						<td><a onclick="borrar_elemento(<?php echo $id_cuerpo; ?>)" ><img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/delete.png" class="img_delete"></a></td>
																					</tr>
																					<?php
																				}

																				if($cuerpo_tipo=="texto") {

																					$array_tamanos[1] = 'Tamaño del texto: Pequeño';
																					$array_tamanos[2] = 'Tamaño del texto: Normal';
																					$array_tamanos[3] = 'Tamaño del texto: Grande';
																					$array_tamanos[4] = 'Tamaño del texto: Enorme';

																					?>
																					<tr>
																						<td width="60"><input type="text" name="orden[<?php echo $id_cuerpo;?>]" maxlength="2" value="<?php echo $i; ?>"></td>
																						<td>
																							<p class="cd-select">
																								<select name="tamano_texto[<?php echo $id_cuerpo;?>]" class="select_class" id="select_subgrupo_1" >
																									<?php foreach ($array_tamanos as $tamano => $tamano_txt) { 
																										$selected = NULL;

																										if($tamano_texto==0) {
																											if($tamano==2) {
																												$selected='selected';
																											}
																										}

																										if($tamano_texto==$tamano) {
																											$selected='selected';
																										}
																										?>
																										<option <?php echo $selected; ?> value="<?php echo $tamano; ?>"><?php echo $tamano_txt; ?></option>		
																										<?php } ?>		
																									</select></p>
																									<textarea name="elemento[<?php echo $id_cuerpo;?>]" id="area<?php echo $i;?>"><?php echo $contenido; ?></textarea></td>
																									<td><a onclick="borrar_elemento(<?php echo $id_cuerpo; ?>)" ><img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/delete.png" class="img_delete"></a></td>
																									<script type="text/javascript">
																										bkLib.onDomLoaded(function() {
																											new nicEditor({buttonList : ['bold','italic',,'forecolor','xhtml']}).panelInstance('area<?php echo $i;?>');
																										});
																									</script>
																								</tr>	  
																								<?php }

																								if($cuerpo_tipo=="video") { ?>
																								<input type="hidden" name="elemento[<?php echo $id_cuerpo;?>]" value="<?php echo $contenido; ?>" >
																								<tr>
																									<td width="60"><input type="text" name="orden[<?php echo $id_cuerpo;?>]" maxlength="2" value="<?php echo $i; ?>"></td>
																									<td>
																										<div class="video_youtube">
																											<div class="video-container">
																												<iframe src="https://www.youtube.com/embed/<?php echo $contenido;?>" frameborder="0" ></iframe>
																											</div>
																										</div>
																									</td>
																									<td><a onclick="borrar_elemento(<?php echo $id_cuerpo; ?>)" ><img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/delete.png" class="img_delete"></a></td>
																								</tr>


																								<?php }
																								$i++;
																							} while($row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo));
																							?>

																						</tbody>
																					</table>
																				</div>
																				<img src="" id="imagen"/>
																				<a class="vc_btn_largo vc_btn_verde vc_btn_3d" onclick="agregar_elemento()">
																					<span class="fa-stack fa-lg pull-left">
																						<i class="fa fa-circle fa-stack-2x"></i>
																						<i class="fa fa-plus fa-stack-1x fa-inverse"></i>
																					</span>
																					<p>Agregar elemento al cuerpo</p>
																				</a>
																				<br><br>

																			</fieldset>	
																		</section>    	

																		<a name="botonGuardar"></a>
																		<div class="alinear_centro">
																			<input type="submit" value="Guardar" id="btn_nueva_categoria">
																		</div>
																	</form>
																</div>
															</div> <!-- .content-wrapper -->
														</main> 
														<?php include('../../includes/pie-general.php');?>
														<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
														<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
														<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->

														<script type="text/javascript">
															var ddData = [
															<?php
															$url_imagen = $Servidor_url.'APLICACION/Imagenes/diarios/';
															$i = 1;
															do { 
																$id_diario = $row_rs_diarios['id_diario'];
																$diario_nombre = $row_rs_diarios['diario_nombre'];
																$diario_imagen = $row_rs_diarios['diario_imagen'];
																$id_provincia = $row_rs_diarios['id_provincia'];


																if($diario_imagen) {
																	$imagen = $url_imagen.$diario_imagen;
																} else {
																	$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
																}

																$selected = "false";

																if($id_diario == $noticia_id_diario) {
																	$selected = "true";
																}

																$descripcion = $array_provincias[$id_provincia];
																?>
																{
																	text: "<?php echo $diario_nombre; ?>",
																	value: <?php echo $id_diario; ?>,
																	selected: <?php echo $selected; ?>,
																	description: "<?php echo $descripcion; ?>",
																	imageSrc: "<?php echo $imagen; ?>"
																	<?php if($i == $totalrow_rs_diarios) {
																		echo '}';
																	} else {
																		echo '},';
																	}

																	$i++; } while($row_rs_diarios = mysql_fetch_assoc($rs_diarios)); ?>
																	];

//Dropdown plugin data


$('#demoBasic').ddslick({
	data: ddData,
	width: 600,
	imagePosition: "left",
	selectText: "Elegí un diario",
	onSelected: function (data) {
		var valor = data.selectedData.value;
		document.getElementById("diario").value = valor;
	}
});	

function validar_formulario() {
	var diario = document.getElementById("diario").value;

	if(diario==0) {
		alert('Tenés que elegir un diario');
		return false;
	} else {
		return true;
	}
}

function guardar_datos() {
	var diario = document.getElementById("diario").value;
	var titulo = document.getElementById("titulo").value;
	var bajada = document.getElementById("bajada").value;
	var palabras_claves = document.getElementById("palabras_claves").value;

	if(diario==0) {
		alert('Tenés que elegir un diario');
		return false;
	} else {
		return true;
	}

}

function agregar_elemento() {
	$('#popup_agregar').addClass('is-visible');
}
function cerrar_popup() {
	$('.cd-popup').removeClass('is-visible');
}

function eliminar_elemento(elemento) {
	$('#elemento_'+elemento).html('');
}

function agregar_imagen() {
	var elemento = document.getElementById("elementos_contador").value;

	cerrar_popup();	
	window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR//00-barra-navegacion/wavi-noticias/popUps/01-cargar-imagen.php?noticia=<?php echo $noticia; ?>&id_administrador=<?php echo $id_administrador;?>','popup','width=400,height=400');

	document.getElementById("elementos_contador").value = parseInt(elemento)+1;		
}

function agregar_video(){
	$('.cd-popup').removeClass('is-visible');
	$('#popup_video').addClass('is-visible');
}

function agregar_texto(){
	$('.cd-popup').removeClass('is-visible');
	$('#popup_texto').addClass('is-visible');
}

function imagen_lista() {
	alert('imagen cargada :D');
}

function confirmar_video_youtube() {
	var video = document.getElementById("input_video").value;
	window.location.href = "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/06-video-youtube.php?noticia=<?php echo $noticia; ?>&video="+video;
}

function confirmar_agregar_video() {
	document.getElementById("formTexto").submit();
}

function borrar_elemento(elemento) {
	$('#popup_borrar_elemento').addClass('is-visible');

	$('#btn_confirmar_borrado_elemento').html('<a onclick="confirmar_borrado_elemento('+elemento+')">Sí</a>');
}

function confirmar_borrado_elemento(elemento) {
	window.location.href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/09-borrar-elemento-cuerpo.php?elemento="+elemento+"&noticia=<?php echo $noticia; ?>";
}

function agregar_direccion() {	
	var direccion_contador = document.getElementById("direcciones_contador").value;
	$('#btn_agregar_direccion').addClass('boton_trabajando');			
	document.getElementById("btn_agregar_direccion").disabled = true;
	$.ajax({
		url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/ajax/agregar-direccion.php?"+"contador="+direccion_contador,
		success: function (resultado) {
			$('#direcciones').append(resultado);
			$('#btn_agregar_direccion').removeClass('boton_trabajando');			
			document.getElementById("btn_agregar_direccion").disabled = false;
			document.getElementById("direcciones_contador").value = parseInt(direccion_contador)+1;		
		}
	});
	
}
function eliminar_direccion(direccion, id_direccion) {
	$('#popup_direccion').addClass('is-visible');
	var variable = '<a onclick="confirmar_borrar_direccion('+direccion+', '+id_direccion+')">Sí</a>';
	$('#btn_confirmar_direccion').html(variable);		
}
function cargar_ciudades(provincia,campo) {	
	if(provincia) {
		$('#select_ciudad_'+campo).html('<option value="0">Cargando ciudades...</option>');
		$.ajax({
			url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/ajax/cargar-ciudades.php?provincia="+provincia,
			success: function (resultado) {
				$('#select_ciudad_'+campo).html(resultado);
			}
		});
	}			
}	
function confirmar_borrar_direccion(direccion, id_direccion) {
	$('.cd-popup').removeClass('is-visible');
	$('#direccion_agregada_'+direccion).html('');
	if(id_direccion) {
		$.ajax({
			url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/ajax/borrar-direccion.php?direccion="+id_direccion,
			success: function (resultado) {
				var variable = resultado;
			}
		});
	}	
}	

function nota_atemporal() {
	var dato = document.getElementById("check_atemporal").checked;

	if(dato) {
		$('#fecha_noticia').hide();
	} else {
		$('#fecha_noticia').show();
	}
}
</script>
</body>
</html>