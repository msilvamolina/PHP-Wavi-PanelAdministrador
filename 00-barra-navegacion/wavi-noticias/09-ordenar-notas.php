<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$pagina= trim($_GET['pagina']);

$limite = 50;
$pagina_get = $pagina;
if(!$pagina_get) {
	$pagina_get=1;
}
if($pagina) {
	$pagina = $pagina-1;
}
$arranca = $pagina*$limite;

conectar2("wavi", "sitioweb");

//consultar en la base de datos
$query_rs_noticias_agrupadas = "SELECT noticias.id_noticia, noticias.noticia_titulo, noticias.id_grupo, noticias.foto_portada, diarios.diario_nombre, diarios.diario_imagen  FROM noticias, diarios WHERE id_grupo > 0 AND noticias.id_diario = diarios.id_diario ORDER BY orden ASC";
$rs_noticias_agrupadas = mysql_query($query_rs_noticias_agrupadas)or die(mysql_error());
$row_rs_noticias_agrupadas = mysql_fetch_assoc($rs_noticias_agrupadas);
$totalrow_rs_noticias_agrupadas = mysql_num_rows($rs_noticias_agrupadas);

do {
	$id_noticia = $row_rs_noticias_agrupadas['id_noticia'];
	$noticia_titulo = $row_rs_noticias_agrupadas['noticia_titulo'];
	$id_grupo = $row_rs_noticias_agrupadas['id_grupo'];

	$foto_portada = $row_rs_noticias_agrupadas['foto_portada'];
	$diario_imagen = $row_rs_noticias_agrupadas['diario_imagen'];

	if(!$array_noticias_grupos[$id_grupo]) {
		$array_noticias_grupos[$id_grupo] = $id_noticia;	
	} else {
		$array_noticias_grupos[$id_grupo] .= '-'.$id_noticia;
	}
	
	$array_noticias_agrupadas[$id_noticia] = $noticia_titulo;
	$array_noticias_foto_portada[$id_noticia] = $foto_portada;
	$array_noticias_diario_imagen[$id_noticia] = $diario_imagen;

} while($row_rs_noticias_agrupadas = mysql_fetch_assoc($rs_noticias_agrupadas));

$pagina_actual_variables = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/04-noticias.php?';
if($usuario) {
	$pagina_actual_variables = $pagina_actual_variables.'usuario='.$usuario.'&';
}
if($q) {
	$pagina_actual_variables = $pagina_actual_variables.'q='.$q.'&';
}
$pagina_siguiente = $pagina+2;
$pagina_anterior = $pagina;
$disabled_siguiente = null;
$disabled_anterior = null;
$link_siguiente = $pagina_actual_variables.'pagina='.$pagina_siguiente;
$link_anterior = $pagina_actual_variables.'pagina='.$pagina_anterior;
if($pagina_anterior<=0) {
	$disabled_anterior = 'disabled';
	$link_anterior = null;
}

if(!$totalrow_rs_noticias) {
	$disabled_siguiente = 'disabled';
	$link_siguiente = null;
}
//consultar en la base de datos
$query_rs_fotos = "SELECT id_foto, recorte_foto_miniatura FROM fotos_publicaciones ";
$rs_fotos = mysql_query($query_rs_fotos)or die(mysql_error());
$row_rs_fotos = mysql_fetch_assoc($rs_fotos);
$totalrow_rs_fotos = mysql_num_rows($rs_fotos);

$ruta = $Servidor_url.'APLICACION/Imagenes/notas/recortes/';

do {
	$id_foto = $row_rs_fotos['id_foto'];
	$nombre_foto = $row_rs_fotos['recorte_foto_miniatura'];

	$array_fotos[$id_foto] = $ruta.$nombre_foto;
} while($row_rs_fotos = mysql_fetch_assoc($rs_fotos));


//consultar en la base de datos
$query_rs_grupo_noticias = "SELECT id_grupo, grupo_nombre FROM grupo_noticias ORDER BY orden ASC ";
$rs_grupo_noticias = mysql_query($query_rs_grupo_noticias)or die(mysql_error());
$row_rs_grupo_noticias = mysql_fetch_assoc($rs_grupo_noticias);
$totalrow_rs_grupo_noticias = mysql_num_rows($rs_grupo_noticias);

do {
	$id_grupo = $row_rs_grupo_noticias['id_grupo'];
	$grupo_nombre = $row_rs_grupo_noticias['grupo_nombre'];

	$array_grupos[$id_grupo] = $grupo_nombre;
} while ($row_rs_grupo_noticias = mysql_fetch_assoc($rs_grupo_noticias));

$array_grupos = array_filter($array_grupos);

desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<link rel='stylesheet' href='<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/styles.css' type='text/css' media='all' />
	<?php include('../../includes/head-general.php'); ?>

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/checkbox/style.css?v=3"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<link href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery-ui.css" rel="stylesheet">
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery-1.7.2.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery-ui.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery.ui.touch-punch.min.js"></script>

	<script type="text/javascript">

		$(function() {
			$( "#test-list" ).sortable({
				placeholder: "ui-state-highlight",
				opacity: 0.6,
				update: function(event, ui) {
					document.getElementById("boton_submit").disabled = true;
					$('#boton_submit').addClass('boton_trabajando');

					var order = $('#test-list').sortable('serialize');
					var sendInfo = {
						order: order,
					};

					$.ajax({
						type: "POST",
						url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/ajax/01-guardar-orden-noticias-db.php",
						success: function (resultado) {
							$('#input_resultado').val(resultado);
							document.getElementById("boton_submit").disabled = false;
							$('#boton_submit').removeClass('boton_trabajando');
						}, data: sendInfo
					});

				}
			});
			$( "#test-list" ).disableSelection(); 

			$( "#test-list" ).sortable({
				cancel: ".ui-state-disabled"
			});

		});

	</script>
	<style type="text/css">
		.rojo {
			color: #F44336;
			font-weight: bold;
		}

		.verde {
			color: #2E7D32;
			font-weight: bold;
		}

		.checkbox {
			z-index: 999;
		}
		h2 {
			font-size: 20px;
		}

		.input_popup {
			width: 100%;
			padding: 10px;
		}

		#agrupadas table td {
			background-color: #FFF9C4;
		}
		#agrupadas table .td_header {
			background-color: #FFC107;
			color: #fff;
		}

		.noticias_principales {
			background:#FC0;
			padding:20px;
			padding-top:1px;
		}


		.noticias_varias {
			background:#ccc;
			padding:20px;
			margin-top:20px;
			padding-top:1px;	
		}

		.appendoButtons{
			font-size:26px;
		}
		#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
		#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
		#sortable li span { position: absolute; margin-left: -1.3em; }

		.botones_noticias a{
			background:#F30;
			color:#fff;
			padding:10px;
			font-weight:bold;
			text-decoration:none;
		}

		.botones_noticias a:hover{
			background:#606;
		}

		.lista_header {
			background:#f90!important; 
			color:#fff;
		}
		#contenedor_boton {
			width: 100%;
			text-align: right;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper" style="max-width:100%">
			<div class="cd-form floating-labels" style="max-width:100%">
				<div id="resultado">
					<form method="POST" id="formQuitarVinculaciones" action="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/18-guardar-noticia-grupo-db.php">
						<input type="hidden" required value="" name="orden" id="input_resultado">
						<div id="contenedor_boton">
							<input type="submit" value="Guardar orden" id="boton_submit" class="boton_submit">
						</div>
					</form>
				</div>
				<?php if($_GET['ok']) { ?>
				<div class="alert alert-success" role="alert">
					Los cambios se guardaron correctamente a las <?php echo date('H:i:s'); ?>
				</div>
				<?php } ?>
				<ul id="test-list">

					<?php foreach ($array_grupos as $id_grupo => $grupo_nombre) { ?>
					<li id="listItem_GRUPO<?php echo $id_grupo; ?>" class="ui-state-disabled lista_header" ><strong><?php echo $grupo_nombre; ?></strong></li>
					<?php if($array_noticias_grupos[$id_grupo]) {
						$array = explode('-', $array_noticias_grupos[$id_grupo]);
						foreach ($array as $id_noticia) {
							$url_diario = $Servidor_url.'APLICACION/Imagenes/diarios/';

							$noticia_titulo = $array_noticias_agrupadas[$id_noticia];

							$foto_portada = $array_noticias_foto_portada[$id_noticia];
							$diario_imagen = $array_noticias_diario_imagen[$id_noticia];

							$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
							$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';

							if($foto_portada) {
								$imagen = $array_fotos[$foto_portada];
							}

							$super_class = null;
							if($total_categorias_vinculadas!=$total_categorias_subgrupo) {
								$super_class = 'categorias_con_subgrupos';
							}
							?>

							<li id="listItem_<?php echo $id_noticia;?>">
								<table class="table table-striped" >
									<tbody>
										<tr class="<?php echo $super_class; ?>" data-href="noticia<?php echo $id_noticia; ?>">
											<td width="60">
												<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/arrow.png" alt="move" width="16" height="16" class="handle" />
											</td>

											<td width="60">
												<?php echo $id_noticia; ?></td>
												<td width="60">
													<img src="<?php echo $url_diario.$diario_imagen; ?>"  width="50"></td>
													<td width="110">
														<img src="<?php echo $imagen; ?>"  width="100"></td>
														<td><?php echo $noticia_titulo; ?></td>
													</tr>	
												</tbody>
											</table>
											<?php }} } ?>

										</ul>
									</div> <!-- .content-wrapper -->
								</main> 
								<script type="text/javascript">
									document.getElementById("boton_submit").disabled = true;
									$('#boton_submit').addClass('boton_trabajando');
								</script>
							</body>
							</html>