<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$pagina= trim($_GET['pagina']);

$limite = 50;
$pagina_get = $pagina;
if(!$pagina_get) {
	$pagina_get=1;
}
if($pagina) {
	$pagina = $pagina-1;
}
$arranca = $pagina*$limite;

conectar2("wavi", "sitioweb");

//consultar en la base de datos
$query_rs_noticias = "SELECT * FROM promociones ORDER BY id_promocion DESC LIMIT $arranca,$limite ";
$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
$totalrow_rs_noticias = mysql_num_rows($rs_noticias);

$pagina_actual_variables = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/01-promociones.php?';
if($usuario) {
	$pagina_actual_variables = $pagina_actual_variables.'usuario='.$usuario.'&';
}
if($q) {
	$pagina_actual_variables = $pagina_actual_variables.'q='.$q.'&';
}
$pagina_siguiente = $pagina+2;
$pagina_anterior = $pagina;
$disabled_siguiente = null;
$disabled_anterior = null;
$link_siguiente = $pagina_actual_variables.'pagina='.$pagina_siguiente;
$link_anterior = $pagina_actual_variables.'pagina='.$pagina_anterior;
if($pagina_anterior<=0) {
	$disabled_anterior = 'disabled';
	$link_anterior = null;
}

if(!$totalrow_rs_noticias) {
	$disabled_siguiente = 'disabled';
	$link_siguiente = null;
}
//consultar en la base de datos
$query_rs_fotos = "SELECT id_foto, id_promocion, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_promocion > 0 AND orden < 2";
$rs_fotos = mysql_query($query_rs_fotos)or die(mysql_error());
$row_rs_fotos = mysql_fetch_assoc($rs_fotos);
$totalrow_rs_fotos = mysql_num_rows($rs_fotos);

$ruta = $Servidor_url.'APLICACION/Imagenes/promos/recortes/';

do {
	$id_foto = $row_rs_fotos['id_foto'];
	$nombre_foto = $row_rs_fotos['recorte_foto_miniatura'];

	$array_fotos[$id_foto] = $ruta.$nombre_foto;
} while($row_rs_fotos = mysql_fetch_assoc($rs_fotos));

desconectar();

conectar2("wavi", "WAVI");
//consultar en la base de datos
$query_rs_grupos = "SELECT id_grupo_categoria, categoria_nombre, categoria_imagen FROM grupo_categorias ";
$rs_grupos = mysql_query($query_rs_grupos)or die(mysql_error());
$row_rs_grupos = mysql_fetch_assoc($rs_grupos);
$totalrow_rs_grupos = mysql_num_rows($rs_grupos);

do {
	$id_grupo_categoria = $row_rs_grupos['id_grupo_categoria'];
	$categoria_nombre = $row_rs_grupos['categoria_nombre'];
	$categoria_imagen = $row_rs_grupos['categoria_imagen'];

	$array_categorias[$id_grupo_categoria]['imagen'] = $categoria_imagen;
	$array_categorias[$id_grupo_categoria]['nombre'] = $categoria_nombre;

} while ($row_rs_grupos = mysql_fetch_assoc($rs_grupos));


//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);
do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];
	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));

//consultar en la base de datos
$query_rs_array_ciudades = "SELECT id_ciudad, ciudad_nombre, id_provincia  FROM ciudades ORDER BY ciudad_nombre ";
$rs_array_ciudades = mysql_query($query_rs_array_ciudades)or die(mysql_error());
$row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades);
$totalrow_rs_array_ciudades = mysql_num_rows($rs_array_ciudades);

do {
	$id_ciudad = $row_rs_array_ciudades['id_ciudad'];
	$ciudad_nombre = $row_rs_array_ciudades['ciudad_nombre'];
	$array_ciudades[$id_ciudad] = $ciudad_nombre;
} while($row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades));
desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->

	<style type="text/css">
		.rojo {
			color: #F44336;
			font-weight: bold;
		}

		.verde {
			color: #2E7D32;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper" style="max-width:100%">
			<!-- Contenido de la Pagina-->
			<nav role="navigation">
				<ul class="cd-pagination">
					<li class="button"><a class="<?php echo $disabled_anterior; ?>" href="<?php echo $link_anterior; ?>">Anterior</a></li>
					<li class="button"><a  class="<?php echo $disabled_siguiente; ?>"  href="<?php echo $link_siguiente; ?>">Siguiente</a></li>
				</ul>
			</nav> <!-- cd-pagination-wrapper -->	
			<div class="cd-form floating-labels" style="max-width:100%">
				<legend id="txt_nueva_categoria">Notas</legend>

				<?php if($totalrow_rs_noticias) { ?>
				<table class="table table-striped">
					<thead class="tabla_encabezado">
						<tr>
							<th><b>#</b></th>
							<th><b>Portada</b></th>
							<th><b>Título</b></th>
							<th style="text-align:center"><b>Categoría</b></th>
							<th><b>Ubicación</b></th>
							<th><b>Publicada</b></th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$url_diario = $Servidor_url.'APLICACION/Imagenes/diarios/';

						do { 
							$id_promocion = $row_rs_noticias['id_promocion'];
							$promocion_titulo = $row_rs_noticias['promocion_titulo'];
							$noticia_publicada = $row_rs_noticias['promocion_publicada'];
							$nombre_foto = $row_rs_noticias['nombre_foto'];
							$fecha_publicacion = $row_rs_noticias['fecha_publicacion'];
							$foto_portada = $row_rs_noticias['foto_portada'];

							$foto_portada = $array_fotos[$foto_portada];

							$diario_imagen = $row_rs_noticias['diario_imagen'];
							$promocion_categoria = $row_rs_noticias['promocion_categoria'];

							$promocion_ciudad = $row_rs_noticias['promocion_ciudad'];
							$promocion_provincia = $row_rs_noticias['promocion_provincia'];

							$publicada = '<p class="rojo">No está publicada</p>';
							if($noticia_publicada) {
								$publicada = '<p class="verde">'.nombre_fecha($fecha_publicacion).'</p>';
							}

							$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
							$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';

							if($foto_portada) {
								$imagen = $foto_portada;
							}

							$super_class = null;
							if($total_categorias_vinculadas!=$total_categorias_subgrupo) {
								$super_class = 'categorias_con_subgrupos';
							}

							$imagen_categoria = $array_categorias[$promocion_categoria]['imagen'];
							$nombre_categoria = $array_categorias[$promocion_categoria]['nombre'];

							$provincia = $array_provincias[$promocion_provincia];
							$ciudad = $array_ciudades[$promocion_ciudad];
							?>
							<tr class="<?php echo $super_class; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/04-ficha-promo.php?promo=<?php echo $id_promocion; ?>">
								<td><?php echo $id_promocion; ?></td>
								<td><img src="<?php echo $imagen; ?>"  width="100"></td>
								<td><?php echo $promocion_titulo; ?></td>
								<td style="text-align:center"><img width="60" src="https://www.mywavi.com/APLICACION/Imagenes/categorias/grandes/<?php echo $imagen_categoria; ?>" class="usuario_avatar"/><br><b><?php echo $nombre_categoria; ?></b>
									<td><strong><?php echo $ciudad; ?></strong>,<br><?php echo $provincia; ?></td>
									<td width="200"><?php echo $publicada; ?></td>
								</tr>		
								<?php } while($row_rs_noticias = mysql_fetch_assoc($rs_noticias)); ?>	          	
							</tbody>
						</table>		     
						<?php } else { ?>
						<p>No hay más notas</p>
						<?php }?>           
					</div>
					<nav role="navigation">
						<ul class="cd-pagination">
							<li class="button"><a class="<?php echo $disabled_anterior; ?>" href="<?php echo $link_anterior; ?>">Anterior</a></li>
							<li class="button"><a  class="<?php echo $disabled_siguiente; ?>"  href="<?php echo $link_siguiente; ?>">Siguiente</a></li>
						</ul>
					</nav> <!-- cd-pagination-wrapper -->	
				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
			<script type="text/javascript">
				$('tr[data-href]').on("click", function() {
					document.location = $(this).data('href');
				});
			</script>
		</body>
		</html>