<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$promo = trim($_POST["promo"]);
$categoria = trim($_POST["categoria"]);
$titulo = trim($_POST["titulo"]);
$provincia = trim($_POST["provincia"]);
$ciudad = trim($_POST["ciudad"]);
$cuerpo = trim($_POST["cuerpo"]);

if(!$titulo || !$promo) {
	$redirigir = $Servidor_url."PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/03-editar-promocion.php?promo=".$promo;
	header('location: '.$redirigir);
	exit;
}

$titulo = arreglar_datos_db($titulo);
$cuerpo = arreglar_datos_db($cuerpo);

conectar2('mywavi', 'sitioweb');

$fecha_actual = date('Y-m-d H:i:s');

mysql_query("UPDATE promociones SET promocion_titulo='$titulo', promocion_categoria='$categoria', promocion_ciudad='$ciudad', promocion_provincia='$provincia', promocion_cuerpo='$cuerpo',
	usuario_que_modifica='$id_administrador',ip_visitante_modificacion='$ip_visitante',fecha_modificacion='$fecha_actual' 
	WHERE id_promocion='$promo'");

desconectar();

$redirigir = $Servidor_url."PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/03-editar-promocion.php?promo=".$promo;
header('location: '.$redirigir);
exit;
?>