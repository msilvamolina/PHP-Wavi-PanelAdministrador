<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$promo = trim($_GET['promo']);
$imagen_cargada = trim($_GET['imagen_cargada']);

if(!$promo) {
	$redireccionar = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/01-promociones.php';
	header('location:'.$redireccionar);
	exit;
}

conectar2("mywavi", "sitioweb");

//consultar en la base de datos
$query_rs_promo = "SELECT * FROM promociones WHERE id_promocion = $promo ";
$rs_promo = mysql_query($query_rs_promo)or die(mysql_error());
$row_rs_promo = mysql_fetch_assoc($rs_promo);
$totalrow_rs_promo = mysql_num_rows($rs_promo);

$titulo = $row_rs_promo['promocion_titulo'];
$palabras_claves = $row_rs_promo['noticia_palabras_claves'];
$fecha_carga = $row_rs_promo['fecha_carga'];
$fecha_modificacion = $row_rs_promo['fecha_modificacion'];
$foto_portada = $row_rs_promo['foto_portada'];

$provincia = $row_rs_promo['promocion_provincia'];
$ciudad = $row_rs_promo['promocion_ciudad'];
$promocion_categoria = $row_rs_promo['promocion_categoria'];

$promocion_cuerpo = revertir_datos_db($row_rs_promo['promocion_cuerpo']);

$noticia_publicada = $row_rs_promo['promocion_publicada'];
$fecha_publicacion = $row_rs_promo['fecha_publicacion'];

	//consultar en la base de datos
$query_rs_imagen = "SELECT id_foto, nombre_foto, fecha_carga, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_promocion = $promo ORDER BY orden ASC ";
$rs_imagen = mysql_query($query_rs_imagen)or die(mysql_error());
$row_rs_imagen = mysql_fetch_assoc($rs_imagen);
$totalrow_rs_imagen = mysql_num_rows($rs_imagen);

do {
	$id_foto = $row_rs_imagen['id_foto'];
	$array_foto[$id_foto] =  $row_rs_imagen['nombre_foto'];
	$array_fecha_carga[$id_foto] = $row_rs_imagen['fecha_carga'];
	$array_recorte_foto_nombre[$id_foto] =  $row_rs_imagen['recorte_foto_nombre'];
	$array_recorte_foto_miniatura[$id_foto] =  $row_rs_imagen['recorte_foto_miniatura'];
} while($row_rs_imagen = mysql_fetch_assoc($rs_imagen));


$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/promos/';

$url_imagen = $Servidor_url.'img/usuarios/grandes/';
$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
$icono_boton = 'add.png';
$link_boton = 'agregar_imagen(1)';

if($foto_portada) {
	$imagen = $ruta_imagenes.$array_foto[$foto_portada];
	$nombre_imagen = $usuario_imagen;	
}

desconectar();

conectar2("mywavi", "WAVI");
//consultar en la base de datos
$query_rs_provincias = "SELECT provincias.provincia_nombre, ciudades.ciudad_nombre  FROM provincias, ciudades WHERE provincias.id_provincia = $provincia AND ciudades.id_ciudad = $ciudad ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);

$provincia = $row_rs_provincias['provincia_nombre'];
$ciudad = $row_rs_provincias['ciudad_nombre'];


//consultar en la base de datos
$query_rs_vinculaciones_negocios = "SELECT vinculacion_negocio_promocion.id_vinculacion, vinculacion_negocio_promocion.id_negocio, negocios.negocio_nombre, negocios.negocio_ciudad, negocios.negocio_provincia FROM vinculacion_negocio_promocion, negocios WHERE vinculacion_negocio_promocion.id_promocion = $promo AND vinculacion_negocio_promocion.id_negocio = negocios.id_negocio ORDER BY vinculacion_negocio_promocion.orden ASC";
$rs_vinculaciones_negocios = mysql_query($query_rs_vinculaciones_negocios)or die(mysql_error());
$row_rs_vinculaciones_negocios = mysql_fetch_assoc($rs_vinculaciones_negocios);
$totalrow_rs_vinculaciones_negocios = mysql_num_rows($rs_vinculaciones_negocios);


//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);
do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];
	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));

//consultar en la base de datos
$query_rs_array_ciudades = "SELECT id_ciudad, ciudad_nombre, id_provincia  FROM ciudades ORDER BY ciudad_nombre ";
$rs_array_ciudades = mysql_query($query_rs_array_ciudades)or die(mysql_error());
$row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades);
$totalrow_rs_array_ciudades = mysql_num_rows($rs_array_ciudades);

do {
	$id_ciudad = $row_rs_array_ciudades['id_ciudad'];
	$ciudad_nombre = $row_rs_array_ciudades['ciudad_nombre'];
	$array_ciudades[$id_ciudad] = $ciudad_nombre;
} while($row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades));

//consultar en la base de datos
$query_rs_categorias = "SELECT categoria_nombre, categoria_imagen FROM grupo_categorias WHERE id_grupo_categoria = $promocion_categoria ";
$rs_categorias = mysql_query($query_rs_categorias)or die(mysql_error());
$row_rs_categorias = mysql_fetch_assoc($rs_categorias);
$totalrow_rs_categorias = mysql_num_rows($rs_categorias);

$categoria_nombre = $row_rs_categorias['categoria_nombre'];
$categoria_imagen = $row_rs_categorias['categoria_imagen'];

desconectar();


$link_negocio = $Servidor_url."PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios-ficha.php?negocio=";
?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/fichas.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->

	<style type="text/css">
		.boton_verde a{
			background: #48b617;
			color: #fff;
		}
		.boton_verde a:hover {
			background: #235d09 !important;
			color: #f6ff05;
		}	
		.boton_rojo a{
			background: #c40000;
			color: #fff;
		}
		.boton_rojo a:hover {
			background: #9e0101 !important;
			color: #f6ff05;
		}
		h3 {
			margin-bottom: 5px;
			font-weight: bold;
		}

		.portada {
			color: #2E7D32;
			font-weight: bold;
		}

		.rojo {
			color: #F44336;
			font-weight: bold;
		}

		.verde {
			color: #2E7D32;
			font-weight: bold;
		}
		a {
			cursor: pointer;
		}
		.input_video {
			width: 100%;
			padding: 10px;
			margin-top: 15px;
		}

		.video-container {
			position: relative;
			padding-bottom: 56.25%;
			padding-top: 30px; height: 0; overflow: hidden;
		}

		.video-container iframe,
		.video-container object,
		.video-container embed {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
		}
		.video_youtube {
			width: 100%;
		}

		.imagen_cuerpo {
			color: #f90;
			font-weight: bold;
		}

	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="cd-popup" id="popup_noticia" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar esta nota?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_noticia"><a onclick="confirmar_borrar_noticia()">Sí</a></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->

			<div class="cd-popup" id="popup_categoria" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar esta imagen?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_categoria"><a onclick="">Sí</a></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->		
			<div class="cd-form floating-labels" style="max-width:1600px">
				<div style="max-width:700px; margin:0 auto;">
					<nav role="navigation">
						<ul class="cd-pagination">
							<li class="button boton_verde"><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/03-editar-promocion.php?promo=<?php echo $promo; ?>">Editar</a></li>		
							<li class="button boton_rojo"><a href="#" onclick="borrar_noticia()">Borrar</a></li>

						</ul>
					</nav> <!-- cd-pagination-wrapper -->
					<section id="crear_categoria" >		
						<fieldset style="margin-top:-50px;">
							<div class="row">
								<div class="col-md-6">
									<div id="imagen_cargando" style="display:none">
										<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/loader.gif">
									</div>
									<div class="imagen_contenedor" id="imagen_contenedor">
										
										<img src="<?php echo $imagen; ?>" class="imagen_usuario">
									</div>					
								</div>
								<div class="col-md-6">
									<div id="txt_usuario_nombre">
										<legend><span><?php echo $titulo; ?></span></legend>
										<?php if($noticia_publicada) { ?>
										<p class="verde">Promoción publicada el <?php echo nombre_fecha($fecha_publicacion); ?> <a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/php/05-publicar-promo.php?publicar=0&promo=<?php echo $promo; ?>">[Quitar publicación]</a></p>
										<?php } else { ?>
										<p class="rojo">Esta promoción no está publicada <a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/php/05-publicar-promo.php?publicar=1&promo=<?php echo $promo; ?>">[Publicar]</a></p>
										<?php } ?>
										<br>
										<p><b>Creada: </b><?php echo nombre_fecha($fecha_carga);?></p>
										<p><b>Última modificación: </b><?php if($fecha_modificacion) { echo nombre_fecha($fecha_modificacion); } else { echo "Nunca se modificó"; } ?></p>
										<p><a href="">Ver nota en el diario</a></p>
									</div>
								</div>
							</div>			
							<table class="table table-striped">
								<tbody>
									<tr>
										<td><b>Categoría</b></td>
										<td><img width="30" src="https://www.mywavi.com/APLICACION/Imagenes/categorias/grandes/<?php echo $categoria_imagen; ?>" /> <?php echo $categoria_nombre; ?></td>
									</tr>
									<tr>
										<td><b>Título</b></td>
										<td><?php echo $titulo; ?></td>
									</tr>
									<tr>
										<td><b>Provincia</b></td>
										<td><?php echo $provincia; ?></td>
									</tr>
									<tr>
										<td><b>Ciudad</b></td>
										<td><?php echo $ciudad; ?></td>
									</tr>
									<tr>
										<td><b>Cuerpo</b></td>
										<td><?php echo $promocion_cuerpo; ?></td>
									</tr>

								</tr>         	

							</tbody>
						</table>	
						<h3>Imágenes
							<a href="#" onclick="agregar_imagen(0)"></a></h3>
							<table class="table table-striped">
								<tbody>
									<?php
									if($totalrow_rs_imagen) { 
										foreach ($array_foto as $id_foto => $nombre_foto) {
											$fecha_carga = $array_fecha_carga[$id_foto];
											$recorte_foto_nombre = $array_recorte_foto_nombre[$id_foto];
											$recorte_foto_miniatura = $array_recorte_foto_miniatura[$id_foto];

											$mostrar_imagen = $ruta_imagenes.$nombre_foto;
											if($recorte_foto_miniatura) {
												$mostrar_imagen = $ruta_imagenes.'recortes/'.$recorte_foto_miniatura;
											}
											?>
											<tr>
												<td width="200"><a target="_blank" href="<?php echo $ruta_imagenes.$nombre_foto; ?>"><img src="<?php echo $mostrar_imagen; ?>" width="200px"></a></td>
												<td>
													<p><a class="rojo" onclick="borrar_imagen(<?php echo $id_foto; ?>)">Borrar imagen</a></p>
													<p><b><?php echo $nombre_foto; ?></b> </p>

													<?php if($foto_portada!=$id_foto) { ?>
													<p><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/php/04-establecer-foto-portada.php?promo=<?php echo $promo; ?>&foto=<?php echo $id_foto; ?>">Establecer como imagen de portada</a><p>
														<?php } else { ?>
														<p class="portada">Portada</p>
														<?php } ?>
														<p><?php echo nombre_fecha($fecha_carga); ?><p><br>
															<?php if($recorte_foto_miniatura) { ?>
															<p><i class="fa fa-crop"></i> Recorte miniatura: <a target="_blank" href="<?php echo $ruta_imagenes.'recortes/'.$recorte_foto_miniatura; ?>"><?php echo $recorte_foto_miniatura; ?></a><p>
																<?php } ?>
																<?php if($recorte_foto_nombre) { ?>
																<p><i class="fa fa-crop"></i> Recorte grande: <a target="_blank" href="<?php echo $ruta_imagenes.'recortes/'.$recorte_foto_nombre; ?>"><?php echo $recorte_foto_nombre; ?></a><p>
																	<?php } ?>
																	<p><a href="<?php echo $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/03-recortar-foto.php?origen=ficha&foto='.$id_foto.'&promo='.$promo; ?>">Recortar foto</a><p>
																	</td>
																</tr>
																<?php }
															} else { ?>
															<tr><td>No hay imágenes</td></tr>
															<?php } ?>
														</tbody>
													</table>	        
													<br><br>
													<h3>Negocios Vinculados</h3>

													<table class="table table-striped">
														<tbody>
															<?php 
															if($totalrow_rs_vinculaciones_negocios) {
																do {
																	$id_vinculacion = $row_rs_vinculaciones_negocios['id_vinculacion'];
																	$id_negocio = $row_rs_vinculaciones_negocios['id_negocio'];
																	$id_grupo_categoria = $row_rs_vinculaciones_negocios['id_grupo_categoria'];
																	$id_subgrupo_categoria = $row_rs_vinculaciones_negocios['id_subgrupo_categoria'];
																	$negocio_nombre = $row_rs_vinculaciones_negocios['negocio_nombre'];
																	$negocio_ciudad = $row_rs_vinculaciones_negocios['negocio_ciudad'];
																	$negocio_provincia = $row_rs_vinculaciones_negocios['negocio_provincia'];

																	$negocio_ciudad = $array_ciudades[$negocio_ciudad];
																	$negocio_provincia = $array_provincias[$negocio_provincia];

																	$variable_grupo_nombre = null;
																	$variable_grupo_nombre .= '<img src="'.$imagen.'" class="usuario_avatar"/><br>';
																	$variable_grupo_nombre .= $grupo_nombre;
																	$variable_grupo_nombre .= '<br><b>'.$subgrupo_nombre.'</b>';
																	?>
																	<tr class="<?php echo $super_class; ?>" data-href="<?php echo $link_negocio.$id_negocio; ?>">
																		<td ><b style="color:red"><?php echo $id_negocio; ?></b></td>
																		<td width="20%" ><?php echo $negocio_nombre; ?></td>
																		<td><strong><?php echo $negocio_ciudad; ?></strong>,<br><?php echo $negocio_provincia; ?></td>

																	</tr>
																	<?php } while($row_rs_vinculaciones_negocios = mysql_fetch_assoc($rs_vinculaciones_negocios)); 
																} else {?>
																<tr><td>No hay negocios vinculados</td></tr>
																<?php } ?>
															</tbody>
														</table>	
														<br><br>

													</div>
												</div> <!-- .content-wrapper -->
											</main> 
											<?php include('../../includes/pie-general.php');?>
											<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
											<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->

											<script type="text/javascript">
												$('tr[data-href]').on("click", function() {
													window.open($(this).data('href'));
												});

												function borrar_imagen(imagen) {
													$('#popup_categoria').addClass('is-visible');
													$('#btn_confirmar_categoria').html('<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/11-borrar-imagen-promo.php?origen=ficha&promo=<?php echo $promo;?>&foto='+imagen+'">Sí</a>');
												}

												function borrar_noticia() {
													$('#popup_noticia').addClass('is-visible');
													$('#btn_confirmar_noticia').html('<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/php/12-borrar-promo.php?promo=<?php echo $promo;?>">Sí</a>');
												}

												function cerrar_popup() {
													$('.cd-popup').removeClass('is-visible');
												}
											</script>
										</body>
										</html>