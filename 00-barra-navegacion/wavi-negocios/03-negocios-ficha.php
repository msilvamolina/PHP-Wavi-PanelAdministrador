<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');
$negocio = trim($_GET['negocio']);
$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-general/03-negocios.php';
if(!$negocio) {
	header('location:'.$redirigir);
	exit;
} 
conectar2('mywavi', 'WAVI');

//consultar en la base de datos
$query_rs_listas = "SELECT id_lista, lista_nombre FROM lista_negocios_sin_conexion ORDER BY id_lista DESC ";
$rs_listas = mysql_query($query_rs_listas)or die(mysql_error());
$row_rs_listas = mysql_fetch_assoc($rs_listas);
$totalrow_rs_listas = mysql_num_rows($rs_listas);

do {
	$id_lista = $row_rs_listas['id_lista'];
	$lista_nombre = $row_rs_listas['lista_nombre'];

	$array_listas[$id_lista] = $lista_nombre;

} while($row_rs_listas = mysql_fetch_assoc($rs_listas));

//consultar en la base de datos
$query_rs_vinculaciones_listas = "SELECT id_lista FROM vinculacion_negocio_lista WHERE id_negocio = $negocio ORDER BY id_lista DESC";
$rs_vinculaciones_listas = mysql_query($query_rs_vinculaciones_listas)or die(mysql_error());
$row_rs_vinculaciones_listas = mysql_fetch_assoc($rs_vinculaciones_listas);
$totalrow_rs_vinculaciones_listas = mysql_num_rows($rs_vinculaciones_listas);

$txt_vinculacion_listas = null;

if($totalrow_rs_vinculaciones_listas) {
	do {
		$id_lista_vinculacion = $row_rs_vinculaciones_listas['id_lista'];

		if(!$txt_vinculacion_listas) {
			$txt_vinculacion_listas = '<br><br><a href="'.$Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/01-negocios-sin-conexion.php?lista='.$id_lista_vinculacion.'">'.$array_listas[$id_lista_vinculacion].'</a>';	
		} else {
			$txt_vinculacion_listas .= ', <a href="'.$Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/01-negocios-sin-conexion.php?lista='.$id_lista_vinculacion.'">'.$array_listas[$id_lista_vinculacion].'</a>';	
		}
		

	} while($row_rs_vinculaciones_listas = mysql_fetch_assoc($rs_vinculaciones_listas));
} else {
	$txt_vinculacion_listas = "<br><br>(Sin vinculaciones)";
}
//consultar en la base de datos
$query_rs_negocio = "SELECT * FROM negocios WHERE id_negocio = $negocio ";
$rs_negocio = mysql_query($query_rs_negocio)or die(mysql_error());
$row_rs_negocio = mysql_fetch_assoc($rs_negocio);
$totalrow_rs_negocio = mysql_num_rows($rs_negocio);
if(!$totalrow_rs_negocio) {
	header('location:'.$redirigir);
	exit;
} 
$id_negocio = $row_rs_negocio['id_negocio'];
$negocio_reportado = $row_rs_negocio['negocio_reportado'];

$negocio_nombre = $row_rs_negocio['negocio_nombre'];
$usuario_que_carga = $row_rs_negocio['usuario_que_carga'];
$fecha_carga = $row_rs_negocio['fecha_carga'];
$id_ciudad = $row_rs_negocio['negocio_ciudad'];
$id_provincia = $row_rs_negocio['negocio_provincia'];
$negocio_categoria = $row_rs_negocio['negocio_categoria'];
$usuario_que_carga = $row_rs_negocio['usuario_que_carga'];

if($negocio_reportado) {
	//consultar en la base de datos
	$query_rs_reporte = "SELECT problema, fecha_reporte FROM negocios_reportados WHERE id_reporte = $negocio_reportado ";
	$rs_reporte = mysql_query($query_rs_reporte)or die(mysql_error());
	$row_rs_reporte = mysql_fetch_assoc($rs_reporte);
	$totalrow_rs_reporte = mysql_num_rows($rs_reporte);
	
	$reporte_problema = $row_rs_reporte['problema'];
	$fecha_reporte = $row_rs_reporte['fecha_reporte'];
	
}
//consultar en la base de datos
$query_rs_ciudad = "SELECT id_ciudad, ciudad_nombre FROM ciudades ";
$rs_ciudad = mysql_query($query_rs_ciudad)or die(mysql_error());
$row_rs_ciudad = mysql_fetch_assoc($rs_ciudad);
$totalrow_rs_ciudad = mysql_num_rows($rs_ciudad);
do {
	$id_ciudad_1 = $row_rs_ciudad['id_ciudad'];
	$ciudad_nombre_1 = $row_rs_ciudad['ciudad_nombre'];
	$array_ciudades[$id_ciudad_1] = $ciudad_nombre_1;
} while($row_rs_ciudad = mysql_fetch_assoc($rs_ciudad));
$ciudad_nombre = $array_ciudades[$id_ciudad];
	//consultar en la base de datos
$query_rs_provincia = "SELECT id_provincia, provincia_nombre  FROM provincias ";
$rs_provincia = mysql_query($query_rs_provincia)or die(mysql_error());
$row_rs_provincia = mysql_fetch_assoc($rs_provincia);
$totalrow_rs_provincia = mysql_num_rows($rs_provincia);
do {
	$id_provincia_1 = $row_rs_provincia['id_provincia'];
	$provincia_nombre_1 = $row_rs_provincia['provincia_nombre'];
	$array_provincias[$id_provincia_1] = $provincia_nombre_1;
} while($row_rs_provincia = mysql_fetch_assoc($rs_provincia));
$provincia_nombre = $array_provincias[$id_provincia];
if($negocio_categoria) {
	//consultar en la base de datos
	$query_rs_categoria = "SELECT id_categoria, categoria_nombre, dependencias  FROM categorias";
	$rs_categoria = mysql_query($query_rs_categoria)or die(mysql_error());
	$row_rs_categoria = mysql_fetch_assoc($rs_categoria);
	$totalrow_rs_categoria = mysql_num_rows($rs_categoria);
	
	do {
		$id_categoria = $row_rs_categoria['id_categoria'];
		$categoria_nombre = $row_rs_categoria['categoria_nombre'];
		$dependencias = $row_rs_categoria['dependencias'];

		if($id_categoria) {
			$array_categoria[$id_categoria] = $categoria_nombre;
			$array_categoria_dependencias[$id_categoria] = $dependencias;
		}
	} while($row_rs_categoria = mysql_fetch_assoc($rs_categoria));
}
//consultar en la base de datos
$query_rs_direcciones = "SELECT id_direccion, direccion_nombre, direccion_numero, direccion_piso, direccion_latitud, direccion_longitud, direccion_ciudad, direccion_provincia FROM direcciones WHERE id_negocio = $negocio ORDER BY id_negocio ASC";
$rs_direcciones = mysql_query($query_rs_direcciones)or die(mysql_error());
$row_rs_direcciones = mysql_fetch_assoc($rs_direcciones);
$totalrow_rs_direcciones = mysql_num_rows($rs_direcciones);
//consultar en la base de datos
$query_rs_telefonos = "SELECT id_telefono, telefono_caracteristica, telefono_direccion, telefono_numero  FROM telefonos WHERE id_negocio = $id_negocio ";
$rs_telefonos = mysql_query($query_rs_telefonos)or die(mysql_error());
$row_rs_telefonos = mysql_fetch_assoc($rs_telefonos);
$totalrow_rs_telefonos = mysql_num_rows($rs_telefonos);
do {
	$id_telefono = $row_rs_telefonos['id_telefono'];
	$telefono_caracteristica = $row_rs_telefonos['telefono_caracteristica'];
	$telefono_direccion = $row_rs_telefonos['telefono_direccion'];
	$telefono_numero = $row_rs_telefonos['telefono_numero'];
	if(!$telefono_direccion) {
		$telefono_direccion = 0;
	}
	if($telefono_numero) {
		if(!$array_direcciones[$telefono_direccion])	{
			$array_direcciones[$telefono_direccion] = '('.$telefono_caracteristica.') '.$telefono_numero;
		} else {
			$array_direcciones[$telefono_direccion] = $array_direcciones[$telefono_direccion].', ('.$telefono_caracteristica.') '.$telefono_numero;			
		}
	}
} while($row_rs_telefonos = mysql_fetch_assoc($rs_telefonos));

//consultar en la base de datos
$query_rs_vinculacion_negocios = "SELECT id_negocio, id_grupo_categoria, id_subgrupo_categoria FROM vinculacion_negocio_categorias WHERE id_negocio = $negocio";
$rs_vinculacion_negocios = mysql_query($query_rs_vinculacion_negocios)or die(mysql_error());
$row_rs_vinculacion_negocios = mysql_fetch_assoc($rs_vinculacion_negocios);
$totalrow_rs_vinculacion_negocios = mysql_num_rows($rs_vinculacion_negocios);

//consultar en la base de datos
$query_rs_grupos_categorias = "SELECT id_grupo_categoria, categoria_nombre, categoria_imagen FROM grupo_categorias ";
$rs_grupos_categorias = mysql_query($query_rs_grupos_categorias)or die(mysql_error());
$row_rs_grupos_categorias = mysql_fetch_assoc($rs_grupos_categorias);
$totalrow_rs_grupos_categorias = mysql_num_rows($rs_grupos_categorias);

do {
	$id_grupo_categoria = $row_rs_grupos_categorias['id_grupo_categoria'];
	$categoria_nombre = $row_rs_grupos_categorias['categoria_nombre'];
	$categoria_imagen = $row_rs_grupos_categorias['categoria_imagen'];

	$array_grupo_nombre[$id_grupo_categoria] = $categoria_nombre;
	$array_grupo_imagen[$id_grupo_categoria] = $categoria_imagen;
} while($row_rs_grupos_categorias = mysql_fetch_assoc($rs_grupos_categorias));

//consultar en la base de datos
$query_rs_subgrupos = "SELECT id_subgrupo, subgrupo_nombre FROM subgrupos_categorias";
$rs_subgrupos = mysql_query($query_rs_subgrupos)or die(mysql_error());
$row_rs_subgrupos = mysql_fetch_assoc($rs_subgrupos);
$totalrow_rs_subgrupos = mysql_num_rows($rs_subgrupos);

do {
	$id_subgrupo = $row_rs_subgrupos['id_subgrupo'];
	$subgrupo_nombre = $row_rs_subgrupos['subgrupo_nombre'];

	$array_subgrupo[$id_subgrupo] = $subgrupo_nombre;
} while($row_rs_subgrupos = mysql_fetch_assoc($rs_subgrupos));

//consultar en la base de datos
$query_rs_usuario = "SELECT id_usuario, usuario_nombre, usuario_apellido FROM usuarios_cargadores ";
$rs_usuario = mysql_query($query_rs_usuario)or die(mysql_error());
$row_rs_usuario = mysql_fetch_assoc($rs_usuario);
$totalrow_rs_usuario = mysql_num_rows($rs_usuario);
do {
	$id_usuario = $row_rs_usuario['id_usuario'];
	$usuario_nombre = $row_rs_usuario['usuario_nombre'];
	$usuario_apellido = $row_rs_usuario['usuario_apellido'];
	$array_usuarios[$id_usuario] = $usuario_nombre.' '.$usuario_apellido;
} while($row_rs_usuario = mysql_fetch_assoc($rs_usuario));

$negocio_categoria = $row_rs_negocio['negocio_categoria'];

$negocio_dependencias = $array_categoria_dependencias[$negocio_categoria];

$negocio_categoria_dependencias = null;
if($negocio_dependencias) {
	$explorar_dependencias = explode('-', $negocio_dependencias);
	foreach ($explorar_dependencias as $categoria) {
		if(!$negocio_categoria_dependencias) {
			$negocio_categoria_dependencias = $array_categoria[$categoria];
		} else {
			$negocio_categoria_dependencias = $negocio_categoria_dependencias.'   &raquo; '.$array_categoria[$categoria];			
		}
	}
	$negocio_categoria_dependencias = $negocio_categoria_dependencias.'   &raquo; '.$array_categoria[$negocio_categoria];			
} else {
	$negocio_categoria_dependencias = $array_categoria[$negocio_categoria];			
}

desconectar();
$usuario_que_carga = $row_rs_negocio['usuario_que_carga'];
$usuario_que_modifica = $row_rs_negocio['usuario_que_modifica'];
$fecha_modificacion = $row_rs_negocio['fecha_modificacion'];
$array_datos['Descripción'] = $row_rs_negocio['negocio_descripcion'];
$array_datos['Palabras Claves'] = $row_rs_negocio['negocio_palabras_claves'];
$array_datos['Provincia']['provincia='.$id_provincia] = $provincia_nombre;
$array_datos['Ciudad']['ciudad='.$id_ciudad] = $ciudad_nombre;
$array_datos['E-mails'] = $row_rs_negocio['negocio_emails'];
$array_datos['Sitio Web'] = $row_rs_negocio['negocio_sitio_web'];
$array_datos['Usuario que cargó']['usuario='.$usuario_que_carga] = $array_usuarios[$usuario_que_carga];
$array_datos['Fecha de carga'] = nombre_fecha_min($row_rs_negocio['fecha_carga']);
if($fecha_modificacion) {
	$array_datos['Usuario que modificó']['usuario='.$usuario_que_modifica] = $array_usuarios[$usuario_que_modifica];
	$array_datos['Fecha modificación'] =  nombre_fecha_min($fecha_modificacion);
}
$link_redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios.php?';
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBcvJODUo2VKA-QuRMAEu8EwYtSNg1cJV0'></script>
	<style type="text/css">
		.tabla_martin {
			width: 100%;
			max-width: 800px;
			margin: 0 auto;
			margin-bottom: 50px;
		}
		.tabla_celda {
			width: 100%;
			text-align: center;
			padding: 10px;
		}	
		.tabla_martin_fila {
			background: #ccc;
		}
		.alinear_derecha {
			text-align: center;
		}
		.alinear_derecha {
			background: #ff6000;
			color: #fff;
			padding: 10px;
		}
		@media only screen and (min-width: 768px) {
			.tabla_celda {
				text-align: left;
				width: 50%;
				float: left;
			}
			.alinear_derecha {
				text-align: right;
			}
		}

		h2 {
			padding-bottom: 0px;
			font-size: 26px;
			text-align: center;
		}
		.vacio {
			color: #c6c6c6;
		}
		.boton_verde a{
			background: #48b617;
			color: #fff;
		}
		.boton_verde a:hover {
			background: #235d09 !important;
			color: #f6ff05;
		}	
		.boton_rojo a{
			background: #c40000;
			color: #fff;
		}
		.boton_rojo a:hover {
			background: #9e0101 !important;
			color: #f6ff05;
		}		
		h2 span {
			background: red;
			padding: 5px;
			border-radius: 5px;
			color: #fff;
		}
		.h2_direcciones {
			padding: 20px;
			padding-bottom: 0px;
		}
		h2 .h2_direcciones {
			margin-top: 40px;
			display: block;
		}
		.clear {
			clear: both;
		}
		.sin_fondo {
			background: #fff !important;
			color: #ff6000!important;
		}
		.fondo_celeste {
			background: #3a86f6 !important;
		}

		.area_texto{
			width: 100%;
		}
		#otro_problema {
			display: none;
		}
		#reportar_problema {
			width: 100%;
			max-width: 300px;
			margin: 0 auto;
		}
		#reportar_problema li{
			padding: 10px;
			background: #790054;
			color: #fff;
			cursor: pointer;
		}
		#reportar_problema li:nth-of-type(2n){
			background: #c400a3;
		}	
		.area_texto {
			color: #000;
			height: 200px;
		}

		.mostrar_otro_poblema {
			display: none;
		}
		#reportar_problema {
			display: none;
		}
		.ul_encabezado {
			color:#ffea00 !important;
			font-size: 20px;
			text-align: center;
		}

		.negocio_reportado {
			width: 100%;
			max-width: 600px;
			margin: 20px auto;
		}	
		.mapa{
			width: 100%;
			height: 300px;
		}	

		.usuario_avatar {
			width: 50px;
			border-radius: 50%;
		}
		td {
			cursor: pointer;
		}

		.fa-toggle-on {
			font-size: 30px;
			color: #03af4f;
			cursor: pointer;
		}
		.fa-toggle-off {
			font-size: 30px;
			color: #f98c96;
			cursor: pointer;
		}	

		a {
			cursor: pointer;
		}

		.select_class {
			width: 100%;
			padding: 10px;
		}

	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<div class="cd-popup" id="popup_agregar_lista" role="alert">
			<div class="cd-popup-container">
				<p>Elegí la lista a la que querés agregar este negocio<br><br>
					<select class="select_class" id="lista_sin_conexion" >
						<option value="0">Ninguna lista seleccionada</option>	

						<?php foreach ($array_listas as $id_lista => $lista_nombre) { ?>
						<option <?php echo $selected; ?> value="<?php echo $id_lista; ?>"><?php echo $lista_nombre; ?></option>	
						<?php  }?>
					</select></p>
					<ul class="cd-buttons" >
						<li><a onclick="confirmar_guardado_lista()">Guardar</a></li>
						<li><a onclick="cerrar_popup()">Cancelar</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->

			<div class="cd-popup" id="popup_borrar" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar este negocio?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_borrado"><a onclick="confirmar_borrado()">Sí</a></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->
			<div class="cd-popup" id="popup_reporte" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer quitarle el reporte al negocio?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_borrado"><a onclick="confirmar_reporte()">Sí</a></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->
			<?php include('../../includes/barra-navegacion.php'); ?>
			<div class="content-wrapper" >
				<!-- Contenido de la Pagina-->
				<nav role="navigation">
					<ul class="cd-pagination">
						<li class="button"><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios-ficha.php?negocio=<?php echo ($negocio-1); ?>">Anterior</a></li>
						<li class="button"><a  href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios-ficha.php?negocio=<?php echo ($negocio+1); ?>">Siguiente</a></li>
					</ul>
				</nav> <!-- cd-pagination-wrapper -->		
				<h2><span><?php echo $id_negocio.'</span><br><br>'.$negocio_nombre; ?></h2>

				<?php if($negocio_reportado) { ?>
				<div class="negocio_reportado">
					<div class="alert alert-danger" role="alert">
						<strong>Atención!</strong> Este negocio fue reportado el <strong><?php echo nombre_fecha_min($fecha_reporte) ?></strong><br>
						<br><strong>El Motivo: </strong> <?php echo $reporte_problema; ?>
						<br><br>Al estar reportado no cuenta en el ranking de negocios, ni se muestra fuera de este sistema.
					</div>
				</div>  

				<center id="btn_reportar_problema">
					<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" style="max-width:300px" onclick="quitar_reporte()">
						<span class="fa-stack fa-lg pull-left">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-bug fa-stack-1x fa-inverse"></i>
						</span>
						<p>Quitar el reporte</p>
					</a>
				</center>

				<?php } else { ?>
				<br><br><br>
				<center id="btn_reportar_problema">
					<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" style="max-width:300px" onclick="reportar_problema()">
						<span class="fa-stack fa-lg pull-left">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-bug fa-stack-1x fa-inverse"></i>
						</span>
						<p>Reportar un problema</p>
					</a>
				</center>

				<div id="problema_cargando" style="display:none">
					<center>
						<img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/loader.gif">
					</center>
				</div>
				<ul id="reportar_problema">
					<li  class="ul_encabezado">¿Qué problema tiene?</li>
					<li onclick="mandar_problema(1)" class="opcion_comun" >Problemas de acentos</li>
					<li onclick="mandar_problema(2)"class="opcion_comun">Latitud y longitud</li>
					<li onclick="mandar_problema(3)"class="opcion_comun">Dirección incompleta</li>
					<li onclick="mandar_problema(4)"class="opcion_comun">Negocio repetido</li>
					<li class="opcion_comun" onclick="mostrar_otro_problema()">Otro</li>
					<li class="mostrar_otro_poblema ">
						<textarea class="area_texto" id="area_texto"></textarea><br><br>
						<div class="alinear_centro">
							<a onclick="mandar_problema(5)" class="boton_azul boton_amarillo" style="padding:8px">Continuar</a>					          	
						</div>
						<br>
					</li>
				</ul>
				<?php } ?>
				<nav role="navigation">
					<ul class="cd-pagination">
						<li class="button boton_verde"><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/06-editar-negocio.php?negocio=<?php echo $negocio; ?>">Editar</a></li>		
						<li class="button boton_rojo"><a href="#" onclick="borrar_negocio()">Borrar</a></li>	
						</ul>
					</nav> <!-- cd-pagination-wrapper -->
					<h2 class="h2_direcciones">Categorías</h2>	
					<?php 
					$link_negocios = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-general/07-grupos-categorias-negocios.php';
					do { 
						$variable_grupo_nombre = null;
						$id_negocio = $row_rs_vinculacion_negocios['id_negocio'];
						$id_grupo_categoria = $row_rs_vinculacion_negocios['id_grupo_categoria'];
						$id_subgrupo_categoria = $row_rs_vinculacion_negocios['id_subgrupo_categoria'];

						$grupo_nombre = $array_grupo_nombre[$id_grupo_categoria];
						$grupo_imagen = $array_grupo_imagen[$id_grupo_categoria];

						if($grupo_imagen) {
							$imagen = $Servidor_url."APLICACION/Imagenes/categorias/grandes/".$grupo_imagen;
						} else {
							$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';           			
						}
						$subgrupo_nombre = $array_subgrupo[$id_subgrupo_categoria];

						if($variable_grupo_nombre) {
							$variable_grupo_nombre .= '<br>';
						}	
						$variable_grupo_nombre .= '<img src="'.$imagen.'" class="usuario_avatar"/><br>';
						$variable_grupo_nombre .= '<a href="'.$link_negocios.'?grupo_categoria='.$id_grupo_categoria.'" target="_blank">'.$grupo_nombre.'</a>';
						$variable_grupo_nombre .= '<br><br><b><a href="'.$link_negocios.'?grupo_categoria='.$id_grupo_categoria.'&subgrupo_categoria='.$id_subgrupo_categoria.'" target="_blank">'.$subgrupo_nombre.'</a></b>';	?>
						<br>
						<center>
							<?php echo $variable_grupo_nombre; ?>
						</center>
						<?php } while($row_rs_vinculacion_negocios = mysql_fetch_assoc($rs_vinculacion_negocios)); ?>	
						<br><br>	
						<div class="tabla_martin">
							<div clas="tabla_martin_fila">
								<div class="tabla_celda alinear_derecha">
									Disponible sin conexión
								</div>
								<div class="tabla_celda"><a onclick="agregar_negocio_lista()">[+] Agregar a lista</a><?php echo $txt_vinculacion_listas; ?></div>
							</div>

							<?php foreach ($array_datos as $nombre => $dato) { ?>
							<div clas="tabla_martin_fila">
								<div class="tabla_celda alinear_derecha sin_fondo"><?php echo $nombre; ?></div>

								<div class="tabla_celda"><?php
									if(is_array($dato)) {
										foreach ($dato as $clave => $valor) {
											if($valor) {
												$explorar_clave = explode('https://', $clave);
												if($explorar_clave[1]) {
													$link = $clave;
												} else {
													$link = $link_redireccion.$clave;
												}
												echo '<a href="'.$link.'">'.$valor.'</a>';
											} else {
												echo '<i class="vacio">(Vacío)</i>';
											}
										}
									} else {
										if($dato) {
											echo $dato;
										} else {
											echo '<i class="vacio">(Vacío)</i>';
										} 
									}	?></div>
								</div>
								<?php } ?>
								<div class="clear"></div>


								<h2 class="h2_direcciones">Direcciones</h2>	
								<nav role="navigation" style="margin-top:-30px">
									<ul class="cd-pagination">
										<li class="button boton_verde"><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/02-cargar-negocio-paso4.php?source=editar&negocio=<?php echo $negocio; ?>">Editar Vinculaciones</a></li>
									</ul>
								</nav> <!-- cd-pagination-wrapper -->		
								<?php if($totalrow_rs_direcciones) { ?>
								<div class="tabla_martin">
									<?php $i=1;
									do {
										$id_direccion = $row_rs_direcciones['id_direccion'];
										$direccion_nombre = $row_rs_direcciones['direccion_nombre'];
										$direccion_numero = $row_rs_direcciones['direccion_numero'];
										$direccion_piso = $row_rs_direcciones['direccion_piso'];
										$direccion_latitud = $row_rs_direcciones['direccion_latitud'];
										$direccion_longitud = $row_rs_direcciones['direccion_longitud'];
										$direccion_ciudad = $row_rs_direcciones['direccion_ciudad'];
										$direccion_provincia = $row_rs_direcciones['direccion_provincia'];	
										$mostrar_direccion = trim($direccion_nombre.' '.$direccion_numero.' '.$direccion_piso);

										$array_longitudes[$i] = $direccion_longitud;
										$array_latitudes[$i] = $direccion_latitud;				
										?>
										<div clas="tabla_martin_fila">
											<div class="tabla_celda alinear_derecha"><?php echo $mostrar_direccion; ?></div>
											<div class="tabla_celda sin_fondo"><?php echo $array_ciudades[$direccion_ciudad].', '.$array_provincias[$direccion_provincia]; ?></div>
											<div class="clear"></div>
										</div>
										<div clas="tabla_martin_fila ">
											<div class="tabla_celda alinear_derecha sin_fondo"><b>Longitud</b></div>
											<div class="tabla_celda"><?php echo $direccion_longitud; ?></div>
											<div class="clear"></div>
										</div>		
										<div clas="tabla_martin_fila sin_fondo">
											<div class="tabla_celda alinear_derecha sin_fondo"><b>Latitud</b></div>
											<div class="tabla_celda"><?php echo $direccion_latitud; ?></div>
											<div class="clear"></div>
										</div>		
										<div clas="tabla_martin_fila sin_fondo">
											<div class="tabla_celda alinear_derecha sin_fondo"><b>Teléfonos</b></div>
											<div class="tabla_celda">
												<?php 
												if($array_direcciones[$id_direccion]) {
													echo $array_direcciones[$id_direccion]; 
												}else {
													echo '<i class="vacio">(Vacío)</i>';
												}
												?></div>
												<div class="clear"></div>
												<?php if($direccion_latitud) { ?>
												<br>
												<center>
													<div class="mapa" id="map_canvas<?php echo $i; ?>"></div><br><br>

													<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" style="max-width:300px"  target="_blank" 
													href="http://maps.google.com/?cbll=<?php echo $direccion_latitud; ?>,<?php echo $direccion_longitud; ?>&cbp=12,20.09,,0,5&layer=c">
													<span class="fa-stack fa-lg pull-left">
														<i class="fa fa-circle fa-stack-2x"></i>
														<i class="fa fa-street-view fa-stack-1x fa-inverse"></i>
													</span>
													<p>Ver en Google Street View</p>
												</a>
											</center><br><br>	
											<?php } ?>
										</div>									
										<?php $i++; } while($row_rs_direcciones = mysql_fetch_assoc($rs_direcciones)); ?>
										<?php } ?>	
										<br><br><br><br>
										<div clas="tabla_martin_fila">
											<div class="tabla_celda alinear_derecha fondo_celeste">Teléfonos sin vinculación</div>
											<div class="tabla_celda">
												<?php 
												if($array_direcciones[0]) {
													echo $array_direcciones[0]; 
												}else {
													echo '<i class="vacio">(Vacío)</i>';
												}
												?></div>
												<div class="clear"></div>
											</div>
										</div>
									</div>
								</div> <!-- .content-wrapper -->
							</main> 
							<?php include('../../includes/pie-general.php');?>
							<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
							<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
<script type="text/javascript">
	function borrar_negocio() {
		$('#popup_borrar').addClass('is-visible');
	}
	function confirmar_borrado() {
		window.open('<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/php/06-borrar-negocio-db.php?negocio=<?php echo $negocio;?>', '_self');
	}
	function cerrar_popup() {
		$('.cd-popup').removeClass('is-visible');
	}

	function quitar_reporte() {
		$('#popup_reporte').addClass('is-visible');
	}

	function confirmar_reporte() {
		//le quitamos el reporte
		window.location.href= '<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/php/07-negocio-quitar-reporte.php?negocio=<?php echo $negocio;?>&reporte=<?php echo $negocio_reportado; ?>';
	}
	function mostrar_otro_problema() {
		$('.mostrar_otro_poblema').show();
		$('.opcion_comun').hide();		
	}

	function reportar_problema() {
		$('#reportar_problema').show();
		$('#btn_reportar_problema').hide();
	}

	function mandar_problema(problema) {
		var txt_problema;

		$('#problema_cargando').show();
		$('#reportar_problema').hide();

		if(problema==1) {
			txt_problema = 'Problemas de acentos'; 
		}
		if(problema==2) {
			txt_problema = 'Latitud y Longitud'; 
		}
		if(problema==3) {
			txt_problema = 'Dirección Incompleta'; 
		}
		if(problema==4) {
			txt_problema = 'Negocio repetido'; 
		}		
		if(problema==5) {
			var txt_problema = document.getElementById("area_texto").value;
		}						

		if(!txt_problema) {
			alert('Tenés que elegir un motivo');
		}
		
		$.ajax({
			url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/php/07-negocios-reportar-problema.php?problema="+txt_problema+"&negocio="+<?php echo $negocio; ?>+"&usuario_que_carga="+<?php echo $usuario_que_carga; ?>,
			success: function (resultado) {
				window.location.reload();			
			}
		});
	}


	<?php if($totalrow_rs_direcciones) { ?>
		var infowindow = new google.maps.InfoWindow({});

		function initialize() {
			geocoder = new google.maps.Geocoder();

			<?php foreach ($array_longitudes as $i => $longitud) { 
				$latitud = $array_latitudes[$i];
				if($latitud && $longitud) {
					?>

					var latlng<?php echo $i;?> = new google.maps.LatLng(<?php echo $latitud; ?>, <?php echo $longitud; ?>);
					var myOptions<?php echo $i;?> = {
						zoom: 18,
						center: latlng<?php echo $i;?>,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					}

					map<?php echo $i;?> = new google.maps.Map(document.getElementById("map_canvas<?php echo $i;?>"), myOptions<?php echo $i;?>);

					var marker = new google.maps.Marker({
						position: latlng<?php echo $i;?>,
						map: map<?php echo $i;?>
					});

					<?php } } ?>

				}


				window.onload = initialize;
				<?php } ?>


				function agregar_negocio_lista() {
					$('#popup_agregar_lista').addClass('is-visible');
				}

				function confirmar_guardado_lista () {
					var id_lista = document.getElementById("lista_sin_conexion").value;
					var id_negocio = <?php echo $id_negocio; ?>;

					if(id_lista>0) {
						window.open("<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/php/05-vincular-negocio-a-lista-db.php?id_lista="+id_lista+"&id_negocio="+id_negocio, "_self");	
					}
				}
			</script>
		</body>
		</html>