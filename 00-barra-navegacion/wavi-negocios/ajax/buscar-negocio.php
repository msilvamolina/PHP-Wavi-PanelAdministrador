<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$busqueda = trim($_GET['nombre']);

if(!$busqueda) {
	exit;
}
conectar2('mywavi', 'WAVI');

	//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);
do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];
	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));

	//consultar en la base de datos
$query_rs_array_ciudades = "SELECT id_ciudad, ciudad_nombre  FROM ciudades ORDER BY ciudad_nombre ";
$rs_array_ciudades = mysql_query($query_rs_array_ciudades)or die(mysql_error());
$row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades);
$totalrow_rs_array_ciudades = mysql_num_rows($rs_array_ciudades);

do {
	$id_ciudad = $row_rs_array_ciudades['id_ciudad'];
	$ciudad_nombre = $row_rs_array_ciudades['ciudad_nombre'];
	$array_ciudades[$id_ciudad] = $ciudad_nombre;	
} while($row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades));

	//consultar en la base de datos
$query_rs_noticias = "SELECT *, MATCH (negocio_nombre, negocio_descripcion, negocio_palabras_claves) AGAINST ( '$busqueda' IN BOOLEAN MODE) AS BUSQUEDA1 FROM negocios WHERE MATCH (negocio_nombre, negocio_descripcion, negocio_palabras_claves) AGAINST ( '$busqueda' IN BOOLEAN MODE) ORDER BY id_negocio DESC";
$rs_negocios = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_negocios = mysql_fetch_assoc($rs_negocios);
$totalrow_rs_noticias = mysql_num_rows($rs_negocios);

do {
	$id_bender_negocio = $row_rs_negocios['id_bender_negocio'];

	$id_negocio = $row_rs_negocios['id_negocio'];
	$negocio_nombre = $row_rs_negocios['negocio_nombre'];
	$negocio_provincia =  $row_rs_negocios['negocio_provincia'];
	$negocio_ciudad =  $row_rs_negocios['negocio_ciudad'];

	$negocio_provincia = $array_provincias[$negocio_provincia];
	$negocio_ciudad = $array_ciudades[$negocio_ciudad];

	$fecha_carga = cuantoHace($row_rs_negocios['fecha_carga']);
	$usuario_que_carga = $row_rs_negocios['usuario_que_carga'];

	$array_negocios[$id_negocio] = $negocio_nombre;
	$array_negocios_bender[$id_negocio] = $id_bender_negocio;
	$array_negocios_provincia[$id_negocio] = $negocio_provincia;
	$array_negocios_ciudad[$id_negocio] = $negocio_ciudad;
	$array_negocios_fecha[$id_negocio] = $fecha_carga;
	$array_negocios_usuario[$id_negocio] = $usuario_que_carga;

} while($row_rs_negocios = mysql_fetch_assoc($rs_negocios));

	//consultar en la base de datos
$query_rs_negocios_like = "SELECT * FROM negocios WHERE negocio_nombre LIKE '$busqueda' ";
$rs_negocios_like = mysql_query($query_rs_negocios_like)or die(mysql_error());
$row_rs_negocios_like = mysql_fetch_assoc($rs_negocios_like);
$totalrow_rs_negocios_like = mysql_num_rows($rs_negocios_like);

$totalrow_rs_noticias = $totalrow_rs_noticias + $totalrow_rs_negocios_like;
do {
	$id_bender_negocio = $row_rs_negocios_like['id_bender_negocio'];

	$id_negocio = $row_rs_negocios_like['id_negocio'];
	$negocio_nombre = $row_rs_negocios_like['negocio_nombre'];
	$negocio_provincia =  $row_rs_negocios_like['negocio_provincia'];
	$negocio_ciudad =  $row_rs_negocios_like['negocio_ciudad'];

	$negocio_provincia = $array_provincias[$negocio_provincia];
	$negocio_ciudad = $array_ciudades[$negocio_ciudad];

	$fecha_carga = cuantoHace($row_rs_negocios_like['fecha_carga']);
	$usuario_que_carga = $row_rs_negocios_like['usuario_que_carga'];

	$array_negocios[$id_negocio] = $negocio_nombre;
	$array_negocios_bender[$id_negocio] = $id_bender_negocio;
	$array_negocios_provincia[$id_negocio] = $negocio_provincia;
	$array_negocios_ciudad[$id_negocio] = $negocio_ciudad;
	$array_negocios_fecha[$id_negocio] = $fecha_carga;
	$array_negocios_usuario[$id_negocio] = $usuario_que_carga;

} while($row_rs_negocios_like = mysql_fetch_assoc($rs_negocios_like));



//consultar en la base de datos
$query_rs_vinculacion_negocios = "SELECT id_negocio, id_grupo_categoria, id_subgrupo_categoria FROM vinculacion_negocio_categorias ";
$rs_vinculacion_negocios = mysql_query($query_rs_vinculacion_negocios)or die(mysql_error());
$row_rs_vinculacion_negocios = mysql_fetch_assoc($rs_vinculacion_negocios);
$totalrow_rs_vinculacion_negocios = mysql_num_rows($rs_vinculacion_negocios);

do {
	$id_negocio = $row_rs_vinculacion_negocios['id_negocio'];
	$id_grupo_categoria = $row_rs_vinculacion_negocios['id_grupo_categoria'];
	$id_subgrupo_categoria = $row_rs_vinculacion_negocios['id_subgrupo_categoria'];

	if($vinculaciones_negocios_grupos[$id_negocio]) {
		if($id_grupo_categoria) {
			$vinculaciones_negocios_grupos[$id_negocio] .= '-'.$id_grupo_categoria.':'.$id_subgrupo_categoria;
		}
	} else {
		$vinculaciones_negocios_grupos[$id_negocio] = $id_grupo_categoria.':'.$id_subgrupo_categoria;
	}
} while($row_rs_vinculacion_negocios = mysql_fetch_assoc($rs_vinculacion_negocios));

//consultar en la base de datos
$query_rs_grupos_categorias = "SELECT id_grupo_categoria, categoria_nombre, categoria_imagen FROM grupo_categorias ";
$rs_grupos_categorias = mysql_query($query_rs_grupos_categorias)or die(mysql_error());
$row_rs_grupos_categorias = mysql_fetch_assoc($rs_grupos_categorias);
$totalrow_rs_grupos_categorias = mysql_num_rows($rs_grupos_categorias);

do {
	$id_grupo_categoria = $row_rs_grupos_categorias['id_grupo_categoria'];
	$categoria_nombre = $row_rs_grupos_categorias['categoria_nombre'];
	$categoria_imagen = $row_rs_grupos_categorias['categoria_imagen'];

	$array_grupo_nombre[$id_grupo_categoria] = $categoria_nombre;
	$array_grupo_imagen[$id_grupo_categoria] = $categoria_imagen;
} while($row_rs_grupos_categorias = mysql_fetch_assoc($rs_grupos_categorias));

//consultar en la base de datos
$query_rs_subgrupos = "SELECT id_subgrupo, subgrupo_nombre FROM subgrupos_categorias";
$rs_subgrupos = mysql_query($query_rs_subgrupos)or die(mysql_error());
$row_rs_subgrupos = mysql_fetch_assoc($rs_subgrupos);
$totalrow_rs_subgrupos = mysql_num_rows($rs_subgrupos);

do {
	$id_subgrupo = $row_rs_subgrupos['id_subgrupo'];
	$subgrupo_nombre = $row_rs_subgrupos['subgrupo_nombre'];

	$array_subgrupo[$id_subgrupo] = $subgrupo_nombre;
} while($row_rs_subgrupos = mysql_fetch_assoc($rs_subgrupos));

//consultar en la base de datos
$query_rs_usuarios = "SELECT id_usuario, usuario_nombre, usuario_apellido FROM usuarios_cargadores ";
$rs_usuarios = mysql_query($query_rs_usuarios)or die(mysql_error());
$row_rs_usuarios = mysql_fetch_assoc($rs_usuarios);
$totalrow_rs_usuarios = mysql_num_rows($rs_usuarios);

do {
	$id_usuario = $row_rs_usuarios['id_usuario'];
	$usuario_nombre = $row_rs_usuarios['usuario_nombre'];
	$usuario_apellido = $row_rs_usuarios['usuario_apellido'];

	$array_usuarios[$id_usuario] = $usuario_nombre;
} while($row_rs_usuarios = mysql_fetch_assoc($rs_usuarios));

desconectar();?>
<center>
	<a style="max-width:300px" class="vc_btn_largo vc_btn_amarillo vc_btn_3d"
	href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/02-cargar-negocio-paso3.php?nombre=<?php echo $busqueda; ?>">
	<span class="fa-stack fa-lg pull-left">
		<i class="fa fa-circle fa-stack-2x"></i>
		<i class="fa fa-edit fa-stack-1x fa-inverse"></i>
	</span>
	<p>Cargar negocio</p>
</a>
</center>
<br>
<?php if($totalrow_rs_noticias) { ?>
<center><p>Se encontraron <b><?php echo $totalrow_rs_noticias; ?></b> resultados</p></center>
<br>	
<table class="table table-striped">
	<thead class="tabla_encabezado">
		<tr>
			<th><b>#</b></th>
			<th><b>Negocio</b></th>
			<th style="text-align:center"><b>Categoría</b></th>
			<th><b>Ubicación</b></th>
			<th><b>Usuario</b></th>
			<th><b>Fecha Carga</b></th>

		</tr>
	</thead>
	<tbody>
		<?php 
		$imagen_bender = $Servidor_url.'PANELADMINISTRADOR/img/usuarios_cargadores/bender-circulo.png';
		foreach ($array_negocios as $id_negocio => $negocio_nombre) {
			if($negocio_nombre) {
				$id_bender_negocio = $array_negocios_bender[$id_negocio];

				$negocio_provincia =  $array_negocios_provincia[$id_negocio];
				$negocio_ciudad =  $array_negocios_ciudad[$id_negocio];

				$fecha_carga = $array_negocios_fecha[$id_negocio];
				$usuario_que_carga = $array_negocios_usuario[$id_negocio];

				$vinculaciones = $vinculaciones_negocios_grupos[$id_negocio];
				$explorar_vinculaciones = explode('-', $vinculaciones);

				$usuario_nombre_txt = $array_usuarios[$usuario_que_carga];
				$variable_grupo_nombre = null;
				foreach ($explorar_vinculaciones as $id_vinculacion) {
					$explorar_grupo_vinculacion = explode(':', $id_vinculacion);

					$id_grupo = $explorar_grupo_vinculacion[0];
					$id_subgrupo = $explorar_grupo_vinculacion[1];

					$grupo_nombre = $array_grupo_nombre[$id_grupo];
					$grupo_imagen = $array_grupo_imagen[$id_grupo];

					$subgrupo_nombre = $array_subgrupo[$id_subgrupo];

					if($grupo_imagen) {
						$imagen = $Servidor_url."APLICACION/Imagenes/categorias/".$grupo_imagen; 
					} else {
						$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';           			
					}

					if($variable_grupo_nombre) {
						$variable_grupo_nombre .= '<br>';
					}	
					$variable_grupo_nombre .= '<img src="'.$imagen.'" class="usuario_avatar"/><br>';
					$variable_grupo_nombre .= $grupo_nombre;
					$variable_grupo_nombre .= '<br><b>'.$subgrupo_nombre.'</b>';

				}
				?>
				<tr class="<?php echo $super_class; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios-ficha.php?negocio=<?php echo $id_negocio; ?>">
					<td><b style="color:red"><?php echo $id_negocio; ?></b></td>
					<td width="20%"><?php echo $negocio_nombre; ?></td>
					<td style="text-align:center"><?php echo $variable_grupo_nombre; ?></td>
					<td><strong><?php echo $negocio_ciudad; ?></strong>,<br><?php echo $negocio_provincia; ?></td>
					<td>
						<?php if($id_bender_negocio) { ?>
						<img class="usuario_avatar" src="<?php echo $imagen_bender; ?>">
						<?php 
						$usuario_nombre_txt = 'Bender y '.$usuario_nombre_txt;
					} ?>
					<img class="usuario_avatar" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/usuarios_cargadores/<?php echo $usuario_que_carga; ?>.jpg">
					<br><b><?php echo $usuario_nombre_txt; ?></b></td>
					<td><strong><?php echo $fecha_carga; ?></strong></td>
				</tr>
				<?php } } ?>   
			</tbody>
		</table>	
		<?php 

	} else { 

		echo '<center><p>No se encontraron resultados</p></center>';
	}	
	?>         
	<script type="text/javascript">
		$('tr[data-href]').on("click", function() {
			document.location = $(this).data('href');
		});
	</script>         