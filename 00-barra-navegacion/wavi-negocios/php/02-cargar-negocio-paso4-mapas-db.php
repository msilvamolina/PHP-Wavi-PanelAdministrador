<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');
require('GridLatLong.php');

if(!$_POST) {
	$link = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios.php';
	header('location:'.$link);
	exit;
}
$negocio = $_POST['id_negocio'];
$source = $_POST['source'];

$nombre_direccion = $_POST['nombre_direccion'];
$latitudes = $_POST['latitud'];
$longitudes = $_POST['longitud'];
$direccion_activa = $_POST['direccion_activa'];

conectar2('mywavi', 'WAVI');

$fecha_modificacion = date('Y-m-d H:i:s');

foreach ($nombre_direccion as $id_direccion => $nombre) {
	if($id_direccion) {
		$nombre = arreglar_datos_db($nombre);
		$latitud = $latitudes[$id_direccion];
		$longitud = $longitudes[$id_direccion];
		$activa = $direccion_activa[$id_direccion];
		$latitud_int = null;
		
		$explorar_nombre = explode(",", $nombre);

		$direccion_nombre = trim($explorar_nombre[0]);

		if(!$activa) {
			$latitud = null;
			$longitud = null;
		}

		if($latitud AND $longitud) {
			$gridlatlong = new JamieMBrown\GridLatLong\GridLatLong();
			$latitud_grief = $gridlatlong->getGridReferences($latitud, $longitud);
			$latitud_int = $latitud_grief[0];
		}

		mysql_query("UPDATE direcciones SET direccion_nombre='$direccion_nombre', direccion_gps_activa='$activa', direccion_nombre_google_maps='$nombre', direccion_latitud='$latitud', direccion_longitud='$longitud', gridref='$latitud_int', usuario_que_modifica='$id_administrador', ip_visitante_modificacion='$ip_visitante', fecha_modificacion='$fecha_modificacion' WHERE id_direccion='$id_direccion'");
	}
}

desconectar();

$link = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/02-cargar-negocio-paso5-1.php?negocio='.$negocio.'&source='.$source;
header('location:'.$link);
exit;
?>