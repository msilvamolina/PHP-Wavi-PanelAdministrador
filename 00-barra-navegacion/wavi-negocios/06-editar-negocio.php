<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');
$negocio = trim($_GET['negocio']);
if(!$negocio) {
	$link = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios.php';
	header('location: '.$link);
	exit;
}

conectar2('mywavi', 'WAVI');
//consultar en la base de datos
$query_rs_negocio = "SELECT id_negocio, negocio_nombre, negocio_descripcion, negocio_palabras_claves, negocio_ciudad, negocio_provincia, negocio_emails, negocio_sitio_web, usuario_que_carga  FROM negocios WHERE id_negocio = $negocio ";
$rs_negocio = mysql_query($query_rs_negocio)or die(mysql_error());
$row_rs_negocio = mysql_fetch_assoc($rs_negocio);
$totalrow_rs_negocio = mysql_num_rows($rs_negocio);
$id_negocio = $row_rs_negocio['id_negocio'];
$negocio_nombre = $row_rs_negocio['negocio_nombre'];
$negocio_descripcion = $row_rs_negocio['negocio_descripcion'];
$negocio_palabras_claves = $row_rs_negocio['negocio_palabras_claves'];
$ciudad = $row_rs_negocio['negocio_ciudad'];
$provincia = $row_rs_negocio['negocio_provincia'];
$negocio_emails = $row_rs_negocio['negocio_emails'];
$negocio_sitio_web = $row_rs_negocio['negocio_sitio_web'];

$usuario_que_carga = $row_rs_negocio['usuario_que_carga'];

if(!$verificar_permiso['permisos_wavi_administrador']) { 
	if($usuario_que_carga != $id_administrador) {
		$link = $Servidor_url.'PANELADMINISTRADOR/errorpermisos.php';
		header('location: '.$link);
		exit;
	}
}

//consultar en la base de datos
$query_rs_ciudad = "SELECT id_ciudad, ciudad_nombre, id_provincia FROM ciudades ORDER BY ciudad_nombre ASC ";
$rs_ciudad = mysql_query($query_rs_ciudad)or die(mysql_error());
$row_rs_ciudad = mysql_fetch_assoc($rs_ciudad);
$totalrow_rs_ciudad = mysql_num_rows($rs_ciudad);
do {
	$id_ciudad = $row_rs_ciudad['id_ciudad'];
	$ciudad_nombre = $row_rs_ciudad['ciudad_nombre'];
	$id_provincia = $row_rs_ciudad['id_provincia'];
	$array_ciudades[$id_ciudad] = $ciudad_nombre;
	if(!$array_ciudades_provincias[$id_provincia]) {
		$array_ciudades_provincias[$id_provincia] = $id_ciudad;
	} else {
		$array_ciudades_provincias[$id_provincia] = $array_ciudades_provincias[$id_provincia].'-'.$id_ciudad;
	}	
} while($row_rs_ciudad = mysql_fetch_assoc($rs_ciudad));

//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);
do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];
	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));
//consultar en la base de datos
$query_rs_direcciones = "SELECT id_direccion, direccion_nombre, direccion_numero, direccion_piso, direccion_ciudad, direccion_provincia  FROM direcciones WHERE id_negocio = $negocio ";
$rs_direcciones = mysql_query($query_rs_direcciones)or die(mysql_error());
$row_rs_direcciones = mysql_fetch_assoc($rs_direcciones);
$totalrow_rs_direcciones = mysql_num_rows($rs_direcciones);
//consultar en la base de datos
$query_rs_telefonos = "SELECT id_telefono, telefono_caracteristica, telefono_numero FROM telefonos WHERE id_negocio = $id_negocio ";
$rs_telefonos = mysql_query($query_rs_telefonos)or die(mysql_error());
$row_rs_telefonos = mysql_fetch_assoc($rs_telefonos);
$totalrow_rs_telefonos = mysql_num_rows($rs_telefonos);
//consultar en la base de datos

//consultar en la base de datos
$query_rs_vinculacion_categoria = "SELECT id_vinculacion, id_grupo_categoria, id_subgrupo_categoria  FROM vinculacion_negocio_categorias WHERE id_negocio = $negocio ORDER BY id_vinculacion ASC";
$rs_vinculacion_categoria = mysql_query($query_rs_vinculacion_categoria)or die(mysql_error());
$row_rs_vinculacion_categoria = mysql_fetch_assoc($rs_vinculacion_categoria);
$totalrow_rs_vinculacion_categoria = mysql_num_rows($rs_vinculacion_categoria);

if($totalrow_rs_vinculacion_categoria) {
	do {
		$id_vinculacion = $row_rs_vinculacion_categoria['id_vinculacion'];
		$id_grupo_categoria = $row_rs_vinculacion_categoria['id_grupo_categoria'];
		$id_subgrupo_categoria = $row_rs_vinculacion_categoria['id_subgrupo_categoria'];

		$array_grupo_categoria[$id_vinculacion] = $id_grupo_categoria;
		$array_subgrupo_categoria[$id_vinculacion] = $id_subgrupo_categoria;
	} while($row_rs_vinculacion_categoria = mysql_fetch_assoc($rs_vinculacion_categoria));
} else {
	$array_grupo_categoria[1] = 0;
	$array_subgrupo_categoria[1] = 0;
	$totalrow_rs_vinculacion_categoria = 1;
}

//consultar en la base de datos
$query_rs_grupo_categoria = "SELECT id_grupo_categoria, categoria_descripcion, categoria_nombre, categoria_imagen FROM grupo_categorias ORDER BY categoria_nombre ASC";
$rs_grupo_categoria = mysql_query($query_rs_grupo_categoria)or die(mysql_error());
$row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria);
$totalrow_rs_grupo_categoria = mysql_num_rows($rs_grupo_categoria);

desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<style type="text/css">
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}
		.txt_categoria_elegida {
			padding: 10px;
			color: #2c97de;
			display: block;
		}
		.grupo_categoria {
			margin-left: -20px!important;
		}

		#section_categoria {
			background: #a7a7a7;
			padding: 30px;
			color: #fff;
		}

		#section_categoria h3 {
			font-size: 24px;
		}
		.select_class {
			background: #eeeeee !important;
		}
		.delete_categoria {
			float: right;
		}
		.delete_categoria img{
			width: 35px;
			margin-top: -5px;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->	
			<div class="cd-popup" id="popup_direccion" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar esta dirección?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_direccion"></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->
			<div class="cd-popup" id="popup_telefono" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar este teléfono?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_telefono"></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->
			<div class="cd-popup" id="popup_categoria" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar esta categoría?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_categoria"></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->
			<div class="cd-form floating-labels">
				<section id="crear_categoria" >							
					<fieldset >
						<legend id="txt_nueva_categoria">Editar negocio</legend>

						<form  onsubmit="return validar_formulario()" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/php/06-editar-negocio-db.php" method="POST">
							<input  type="hidden" id="categoria_contador" value='<?php echo $totalrow_rs_vinculacion_categoria + 1; ?>'>

							<?php $i=1; foreach ($array_grupo_categoria as $id_vinculacion => $id_grupo) { ?>
							<div id="nueva_categoria_<?php echo $i;?>">
								<section id="section_categoria">
									<input type="hidden" value="<?php echo $id_grupo; ?>" name="select_grupo_categoria[]" id="select_grupo_categoria_<?php echo $i; ?>" />
									<a class="delete_categoria" onclick="eliminar_categoria(<?php echo $i;?>)"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a>    				    				    		
									<h3>Elegí una categoría y subcategoría</h3>

									<div id="demoBasic<?php echo $i; ?>"  ></div>		

									<p class="cd-select">
										<select name="subgrupo[]" class="select_class" id="select_subgrupo_<?php echo $i; ?>" >
											<option value="0">Elegí una subcategoría</option>						
										</select></p>
									</section>
									<br><br>
								</div>
								<?php $i++;} ?>
								<div id="div_agregar_categorias"></div>
								<div class="alinear_centro">
									<a class="boton_azul boton_amarillo" onclick="agregar_categoria()" id="btn_agregar_categoria">[+] Agregar a otra categoría</a>
								</div>
								<br><br>
								<input  type="hidden" name="negocio" value='<?php echo $negocio; ?>'>

								<section id="section_categoria">
									<h3>Elegí una provincia</h3><br>
									<p class="cd-select">
										<select name="provincia" class="select_class" required name="provincia" onchange="cargar_ciudades(this.value,0)">									
											<option value="0">Elegí una provincia</option>
											<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
												$elegido = null;
												if($id_provincia==$provincia) {
													$elegido = 'selected';
												}
												if($id_provincia) {
													echo '<option value="'.$id_provincia.'" '.$elegido.'>'.$provincia_nombre.'</option>';
												}
											}
											?>
										</select></p>
										<br><br>
										<h3>Elegí una ciudad</h3><br>

										<p class="cd-select">
											<select  class="select_class" required name="ciudad" id="select_ciudad_0" >									
												<option value="0">Elegí una ciudad</option>
												<?php foreach ($array_ciudades as $id_ciudad => $ciudad_nombre) {
													$elegido = null;
													if($id_ciudad==$ciudad) {
														$elegido = 'selected';
													}
													if($id_ciudad) {
														echo '<option value="'.$id_ciudad.'" '.$elegido.'>'.$ciudad_nombre.'</option>';
													}
												}
												?>
											</select></p>		
										</section>		
										<br>    			    
										<div class="icon">
											<label class="cd-label" for="cd-company">Nombre del negocio</label>
											<input class="company" value="<?php echo $negocio_nombre; ?>" type="text" name="nombre" id="nueva_categoria_nombre" required>
										</div> 			    
										<div class="icon">
											<label class="cd-label" for="cd-textarea">Descripción del negocio</label>
											<textarea class="message" name="descripcion" id="nueva_categoria_descripcion" ><?php echo $negocio_descripcion; ?></textarea>
										</div>
										<div class="icon">
											<label class="cd-label" for="cd-email">Sitio web</label>
											<input class="email" type="text"  value="<?php echo $negocio_sitio_web; ?>" name="sitio_web" id="cd-email">
										</div>			
										<div class="icon">
											<label class="cd-label" for="cd-company">Palabras claves</label>
											<input class="company" type="text" value="<?php echo $negocio_palabras_claves; ?>"  name="palabras_claves" id="nueva_categoria_palabras_claves" required>
										</div> 	
										<div class="icon">
											<label class="cd-label" for="cd-email">E-mails</label>
											<input class="email" type="email" value="<?php echo $negocio_emails; ?>"  name="email" id="cd-email">
										</div>				    	    
										<legend id="txt_nueva_categoria">Direcciones</legend>
										<div id="direcciones">	
											<?php 
											$i = 1;
											do {
												$id_direccion = $row_rs_direcciones['id_direccion'];
												$direccion_nombre = $row_rs_direcciones['direccion_nombre'];
												$direccion_numero = $row_rs_direcciones['direccion_numero'];
												$direccion_piso = $row_rs_direcciones['direccion_piso'];
												$direccion_ciudad = $row_rs_direcciones['direccion_ciudad'];
												$direccion_provincia = $row_rs_direcciones['direccion_provincia'];

												$direccion_nombre = $direccion_nombre.' '.$direccion_numero.' '.$direccion_piso;
												$direccion_nombre = trim($direccion_nombre);
												if(!$id_direccion) {
													$id_direccion = 0;
												}
												?>	
												<div id="direccion_agregada_<?php echo $i; ?>">
													<table width="100%" >
														<tr>
															<td >
																<p class="cd-select icon">
																	<select name="select_provincia[<?php echo $i;?>]" class="budget"  onchange="cargar_ciudades(this.value,<?php echo $i; ?>)">									
																		<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
																			$elegido = null;
																			if($id_provincia==$direccion_provincia) {
																				$elegido = 'selected';
																			}
																			if($id_provincia) {
																				echo '<option value="'.$id_provincia.'" '.$elegido.'>'.$provincia_nombre.'</option>';
																			}
																		}
																		?>
																	</select></p>						    	
																</td>
																<td >
																	<p class="cd-select icon">
																		<select name="select_ciudad[<?php echo $i;?>]" class="budget" id="select_ciudad_<?php echo $i; ?>" >									
																			<?php 
																			$explorar_ciudades = explode('-', $array_ciudades_provincias[$direccion_provincia]);
																			foreach ($explorar_ciudades as $id_ciudad) {
																				$elegido = null;
																				$ciudad_nombre = $array_ciudades[$id_ciudad];
																				if($id_ciudad==$direccion_ciudad) {
																					$elegido = 'selected';
																				}
																				if($id_ciudad) {
																					echo '<option value="'.$id_ciudad.'" '.$elegido.'>'.$ciudad_nombre.'</option>';
																				}
																			}
																			?>
																		</select></p>					    	
																	</td>		
																	<td class="td_delete"><a onclick="eliminar_direccion(<?php echo $i; ?>, <?php echo $id_direccion; ?>)"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a></td>					    				    		
																</tr>
															</table>					
															<table width="100%" >
																<tr>
																	<td width="60%">
																		<input  type="hidden" name="direccion_id[<?php echo $i;?>]" value="<?php echo $id_direccion; ?>"  >							
																		<input  type="text" name="direccion_nombre[<?php echo $i;?>]" value="<?php echo $direccion_nombre; ?>" required placeholder="Dirección" >							

																	</td>						    				    		
																</tr>
																<tr><td>&nbsp;</td></tr>
															</table>
														</div>
														<?php $i++; } while($row_rs_direcciones = mysql_fetch_assoc($rs_direcciones)); ?>

													</div>
													<input  type="hidden" id="direcciones_contador" value='<?php echo $i; ?>'>
													<input  type="hidden" id="borrar_direccion" value='0'>
													<div class="alinear_centro">
														<a class="boton_azul boton_amarillo" onclick="agregar_direccion()" id="btn_agregar_direccion">[+] Agregar otra dirección</a>
													</div>
													<legend id="txt_nueva_categoria">Teléfonos</legend>
													<table width="100%" id="telefonos">
														<?php $i = 1; do {
															$id_telefono = $row_rs_telefonos['id_telefono'];
															$telefono_caracteristica = $row_rs_telefonos['telefono_caracteristica'];
															$telefono_numero = $row_rs_telefonos['telefono_numero']; 

															if(!$id_telefono) {
																$id_telefono = 0;
															}
															?>
															<tr id="telefono_agregado_<?php echo $i; ?>">
																<td>
																	<input  type="hidden" name="telefono_id[]" value="<?php echo $id_telefono; ?>" >							
																	<input  type="text" name="cod_area[]" value="<?php echo $telefono_caracteristica; ?>" required placeholder="Código de área" >							

																</td>
																<td>
																	<input  type="text" name="telefono[]" value="<?php echo $telefono_numero; ?>" required placeholder="Teléfono">							
																</td>		
																<td class="td_delete"><a onclick="eliminar_telefono(<?php echo $i; ?>, <?php echo $id_telefono; ?>)"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a></td>	    				    				    		
															</tr>

															<?php $i++; } while($row_rs_telefonos = mysql_fetch_assoc($rs_telefonos)) ; ?> 
														</table>  
														<input  type="hidden" id="telefonos_contador" value='<?php echo $i; ?>'>
														<div class="alinear_centro">
															<a class="boton_azul boton_amarillo" onclick="agregar_telefonos()">[+] Agregar teléfono</a>
														</div>				    
														<br><br><br>
														<div class="alinear_centro">
															<input type="submit" value="Continuar" id="btn_nueva_categoria">
														</div>
													</form>
												</fieldset>	
											</section>    	

										</div>
									</div> <!-- .content-wrapper -->
								</main> 
								<?php include('../../includes/pie-general.php');?>
								<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
								<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
								<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->

								<script type="text/javascript">

//Dropdown plugin data
var ddData = [
<?php
$url_imagen = $Servidor_url."APLICACION/Imagenes/categorias/grandes/";
$i = 1;
$q=0;
do { 
	$id = $row_rs_grupo_categoria['id_grupo_categoria'];
	$nombre = $row_rs_grupo_categoria['categoria_nombre'];
	$imagen = $row_rs_grupo_categoria['categoria_imagen'];
	$descripcion = $row_rs_grupo_categoria['categoria_descripcion'];

	$array_index_grupo[$id] = $q;
	$selected = 'false';

	if($id==$busqueda_grupo_categoria) {
		$selected = 'true';		
	}

	if($imagen) {
		$imagen = $url_imagen.$imagen;
	} else {
		$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
	}

	$descripcion = substr($descripcion, 0, 100);
	$descripcion .= '...';
	?>
	{
		text: "<?php echo $nombre; ?>",
		value: <?php echo $id; ?>,
		selected: <?php echo $selected; ?>,
		description: "<?php echo $descripcion; ?>",
		imageSrc: "<?php echo $imagen; ?>"
		<?php if($i == $totalrow_rs_grupo_categoria) {
			echo '}';
		} else {
			echo '},';
		}

		$i++;$q++; } while($row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria)); ?>
		];

//Dropdown plugin data


<?php $i=1; foreach ($array_grupo_categoria as $id_vinculacion => $id_grupo) { 
	$index = $array_index_grupo[$id_grupo];
	$subgrupo = $array_subgrupo_categoria[$id_vinculacion];
	?>
	$('#demoBasic<?php echo $i; ?>').ddslick({
		data: ddData, 
		<?php if($id_grupo) { ?>
			defaultSelectedIndex:<?php echo $index; ?>,
			<?php } ?>	
			width: 540,
			imagePosition: "left",
			selectText: "Elegí una categoría",
			onSelected: function (data) {
				var valor = data.selectedData.value;
				cargar_subgrupo(valor, <?php echo $i; ?>, <?php echo $subgrupo; ?>);
			}
		});	
	<?php $i++;} ?>
	function cargar_subgrupo(categoria, numero, subgrupo_elegido) {
		document.getElementById("select_grupo_categoria_"+numero).value = categoria;
		var resultado = '<option value="0">Cargando subcategorías...</option>';
		$('#select_subgrupo_'+numero).html(resultado);
		$.ajax({
			url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/bender/json/03-categorias-subgrupos.php?categoria="+categoria+"&subgrupo="+subgrupo_elegido,
			success: function (resultado) {
				$('#select_subgrupo_'+numero).html(resultado);
			}
		});	
	}

	function agregar_categoria() {
		var categoria_contador = document.getElementById("categoria_contador").value;
		$('#btn_agregar_categoria').addClass('boton_trabajando');			
		document.getElementById("btn_agregar_categoria").disabled = true;

		$.ajax({
			url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/ajax/agregar-categoria.php?numero="+categoria_contador,
			success: function (resultado) {
				$('#div_agregar_categorias').append(resultado);	
				document.getElementById("categoria_contador").value = parseInt(categoria_contador)+1;	
				$('#btn_agregar_categoria').removeClass('boton_trabajando');			
				document.getElementById("btn_agregar_categoria").disabled = false;				
			}
		});		
	}

	function eliminar_categoria(categoria) {
		$('#popup_categoria').addClass('is-visible');
		var variable = '<a onclick="confirmar_borrar_categoria('+categoria+')">Sí</a>';
		$('#btn_confirmar_categoria').html(variable);		
	}

	function confirmar_borrar_categoria(categoria) {
		$('#nueva_categoria_'+categoria).html('');
		$('#popup_categoria').removeClass('is-visible');		
	}

	function agregar_telefonos() {
		var telefonos_contador = document.getElementById("telefonos_contador").value;
		var variable = '<tr id="telefono_agregado_'+telefonos_contador+'">';
		variable = variable+'<td><input  type="text" name="cod_area[]" value="0381" required placeholder="Código de área" ></td>';
		variable = variable+'<td><input  type="text" name="telefono[]" required placeholder="Teléfono"></td>';
		variable = variable+'<td class="td_delete"><a onclick="eliminar_telefono('+telefonos_contador+')"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a></td></tr>';
		
		document.getElementById("telefonos_contador").value = parseInt(telefonos_contador)+1;		
		$('#telefonos').append(variable);
	}	
	function cargar_ciudades(provincia,campo) {	
		if(provincia) {
			$('#select_ciudad_'+campo).html('<option value="0">Cargando ciudades...</option>');
			$.ajax({
				url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/ajax/cargar-ciudades.php?provincia="+provincia,
				success: function (resultado) {
					$('#select_ciudad_'+campo).html(resultado);
				}
			});
		}			
	}	
	function agregar_direccion() {	
		var direccion_contador = document.getElementById("direcciones_contador").value;
		$('#btn_agregar_direccion').addClass('boton_trabajando');			
		document.getElementById("btn_agregar_direccion").disabled = true;
		var provincia = <?php echo $provincia; ?>;
		var ciudad = <?php echo $ciudad; ?>;
		$.ajax({
			url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/ajax/agregar-direccion.php?provincia="+provincia+"&ciudad="+ciudad+"&contador="+direccion_contador,
			success: function (resultado) {
				$('#direcciones').append(resultado);
				$('#btn_agregar_direccion').removeClass('boton_trabajando');			
				document.getElementById("btn_agregar_direccion").disabled = false;
				document.getElementById("direcciones_contador").value = parseInt(direccion_contador)+1;		
			}
		});

	}
	function eliminar_direccion(direccion, id_direccion) {
		$('#popup_direccion').addClass('is-visible');
		var variable = '<a onclick="confirmar_borrar_direccion('+direccion+', '+id_direccion+')">Sí</a>';
		$('#btn_confirmar_direccion').html(variable);		
	}
	function eliminar_telefono(telefono, id_telefono) {
		$('#popup_telefono').addClass('is-visible');
		var variable = '<a onclick="confirmar_borrar_telefono('+telefono+', '+id_telefono+')">Sí</a>';
		$('#btn_confirmar_telefono').html(variable);		
	}	
	function cerrar_popup() {
		$('.cd-popup').removeClass('is-visible');
	}
	function confirmar_borrar_direccion(direccion, id_direccion) {
		$('.cd-popup').removeClass('is-visible');
		$('#direccion_agregada_'+direccion).html('');
		if(id_direccion) {
			$.ajax({
				url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/ajax/borrar-direccion.php?direccion="+id_direccion,
				success: function (resultado) {
					var variable = resultado;
				}
			});
		}	
	}
	function confirmar_borrar_telefono(telefono, id_telefono) {
		$('.cd-popup').removeClass('is-visible');
		$('#telefono_agregado_'+telefono).html('');
		if(id_telefono) {
			$.ajax({
				url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/ajax/borrar-telefonos.php?telefono="+id_telefono,
				success: function (resultado) {
					var variable = resultado;
				}
			});
		}			
	}	
	function validar_formulario() {
		var error = null;	
		var total = document.getElementsByName('select_grupo_categoria[]').length;
		for (var i = 1; i <= total; i++) {
			var dato =  document.getElementById('select_grupo_categoria_'+i).value;
			if((!dato)||(dato==0)) {
				error = 'No podés dejar categorías vacías';
			}
		}
		var total2 = document.getElementsByName('subgrupo[]').length;
		for (var i = 1; i <= total; i++) {
			var dato =  document.getElementById('select_subgrupo_'+i).value;
			if((!dato)||(dato==0)) {
				error = 'No podés dejar subcategorías vacías';
			}
		}	

		if(error) {
			alert(error);
			return false;	
		} else {
			return true;
		}

	}
</script>
</body>
</html>