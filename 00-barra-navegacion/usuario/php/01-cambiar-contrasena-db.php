<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';

$cambiar_contrasena = 1;
$actualizar_datos = 1;

include('../../../php/verificar-permisos.php');

$contrasena_actual = trim($_POST['contrasena_actual']);
$nueva_contrasena = trim($_POST['nueva_contrasena']);
$repetir_contrasena = trim($_POST['repetir_contrasena']);

$link = $Servidor_url.'sistemaV3/00-barra-navegacion/usuario/01-cambiar-contrasena.php';
$link_ok = $Servidor_url.'sistemaV3/00-barra-navegacion/usuario/01-cambiar-contrasena-ok.php';

if( (!$contrasena_actual) OR (!$nueva_contrasena) OR (!$repetir_contrasena) ) {
	header('location: '.$link.'?error=1');
	exit;
}


if( $nueva_contrasena != $repetir_contrasena) {
	header('location: '.$link.'?error=contrasenas_nuevas');
	exit;
}

conectar2('paradigm', 'usuarios');

//consultar en la base de datos
$query_rs_usuario = "SELECT usuario_contrasena FROM usuarios WHERE id_usuario = $id_administrador ";
$rs_usuario = mysql_query($query_rs_usuario)or die(mysql_error());
$row_rs_usuario = mysql_fetch_assoc($rs_usuario);
$totalrow_rs_usuario = mysql_num_rows($rs_usuario);

$usuario_contrasena = $row_rs_usuario['usuario_contrasena'];
$usuario_contrasena2 = sha1($usuario_contrasena);

$fecha_modificacion = date('Y-m-d H:i:s');
if(($usuario_contrasena != $contrasena_actual) AND ($usuario_contrasena2 != $contrasena_actual)) {
	header('location: '.$link.'?error=contrasena_actual');
	exit;
}

	mysql_query("UPDATE usuarios SET usuario_contrasena='$nueva_contrasena', ultimo_cambio_contrasena='$fecha_modificacion', pedir_cambio_contrasena='0' WHERE id_usuario='$id_administrador'");

desconectar();

	header('location: '.$link_ok);
	exit;
?>