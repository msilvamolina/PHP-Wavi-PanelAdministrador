<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';

$cambiar_contrasena = 1;
$actualizar_datos = 1;

include('../../php/verificar-permisos.php');

?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->

	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
	.td_delete {
		padding: 10px;
		text-align: right;
		width: 30px;
	}
	.td_delete img {
		width: 30px;
		display: block;
	}

	.tabla {
		width: 100%;
	}
	.tabla tr td{
		padding: 10px;
	}	

	.tabla tr:nth-of-type(2n) {
		background: #f5e5f2;
	}

	</style>
</head>
<body>
<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
	<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
		<!-- Contenido de la Pagina-->
		<div class="cd-popup" id="popup_contrasena" role="alert">
	<div class="cd-popup-container">
		<p>Las contraseñas ingresadas, tienen que ser iguales</p>
		<ul class="cd-buttons">
			<li style="width:100%"><a onclick="cerrar_popup()">Bueno</a></li>
		</ul>
		<a href="#0" class="cd-popup-close img-replace"></a>
	</div> <!-- cd-popup-container -->
</div> <!-- cd-popup -->		
			<div class="cd-form floating-labels" style="max-width:1600px">
			<div style="max-width:600px; margin:0 auto;">
			<section id="crear_categoria" >							
				<fieldset >
					<form onsubmit="return comprobar()" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/usuario/php/01-cambiar-contrasena-db.php" method="post">
					<legend id="txt_nueva_categoria">Cambiar Contraseña</legend>
					<?php if($_GET['sistema']) { ?>				    
						<div class="alert alert-warning" role="alert">
						    <strong><?php echo $administrador_nombre; ?>, </strong>  el sistema ha detectado que estás usando la contraseña que te dimos por defecto. Para continuar en el sistema, por favor cambiala
						</div>
				    <?php } ?>
				    <?php if($_GET['error'] == 'contrasena_actual') { ?>
				    <div class="error-message">
		              <p>La contraseña que ingresaste como actual, no coincide con la guardada en la base de datos</p>
		            </div>
		            <?php } ?>
		 		    <?php if($_GET['error'] == 'contrasenas_nuevas') { ?>
				    <div class="error-message">
		              <p>Las contraseñas nuevas, tienen que ser iguales</p>
		            </div>
		            <?php } ?>      
		 		    <?php if($_GET['error'] == 1) { ?>
				    <div class="error-message">
		              <p>No podés dejar campos vacíos</p>
		            </div>
		            <?php } ?>   		                 
				    <div class="icon">
				    	<label class="cd-label" for="cd-company">Contraseña Actual</label>
						<input class="company" type="password" name="contrasena_actual" id="contrasena_actual" required>
				    </div> 
				    <br>
				    <div class="icon">
				    	<label class="cd-label" for="cd-company">Nueva Contraseña</label>
						<input class="company" type="password" name="nueva_contrasena" id="nueva_contrasena" required>
				    </div> 
				    <div class="icon">
				    	<label class="cd-label" for="cd-company">Repetir Contraseña</label>
						<input class="company"  type="password" name="repetir_contrasena" id="repetir_contrasena" required>
				    </div> 
		    			    
						<div class="alinear_centro">
				      	<input type="submit" value="Guardar Cambios" >
				    </div>
				    </form>

				    </fieldset>	
				</section>    	
 				 </div>	
			</div>
		</div> <!-- .content-wrapper -->
	</main> 
<?php include('../../includes/pie-general.php');?>
<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
<script type="text/javascript">

	function cerrar_popup() {
		$('.cd-popup').removeClass('is-visible');
	}

	function comprobar() {
		var nueva_contrasena = document.getElementById("nueva_contrasena").value; 
		var repetir_contrasena = document.getElementById("repetir_contrasena").value;

		if(nueva_contrasena != repetir_contrasena) { 
			$('#popup_contrasena').addClass('is-visible');
			return false;
		} else {
			return true;
		}
	}
</script>
</body>
</html>