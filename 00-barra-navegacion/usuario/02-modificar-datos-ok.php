<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';

include('../../php/verificar-permisos.php');

conectar2('paradigm', 'usuarios');
//consultar en la base de datos
$query_rs_usuario = "SELECT usuario_usuario, usuario_nombre, usuario_apellido, usuario_email, usuario_fecha_nacimiento, usuario_telefono  FROM usuarios WHERE id_usuario = $id_administrador ";
$rs_usuario = mysql_query($query_rs_usuario)or die(mysql_error());
$row_rs_usuario = mysql_fetch_assoc($rs_usuario);
$totalrow_rs_usuario = mysql_num_rows($rs_usuario);

$usuario_usuario = $row_rs_usuario['usuario_usuario'];
$usuario_nombre = $row_rs_usuario['usuario_nombre'];
$usuario_apellido = $row_rs_usuario['usuario_apellido'];
$usuario_email = $row_rs_usuario['usuario_email'];
$usuario_fecha_nacimiento = $row_rs_usuario['usuario_fecha_nacimiento'];
$usuario_telefono = $row_rs_usuario['usuario_telefono'];

desconectar();
?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>sistemaV3/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>sistemaV3/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>sistemaV3/css/negocios.css"> <!-- Resource style -->

	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
	.td_delete {
		padding: 10px;
		text-align: right;
		width: 30px;
	}
	.td_delete img {
		width: 30px;
		display: block;
	}

	.tabla {
		width: 100%;
	}
	.tabla tr td{
		padding: 10px;
	}	

	.tabla tr:nth-of-type(2n) {
		background: #f5e5f2;
	}
	.full_width {
		width: 100% !important;
	}
	</style>
</head>
<body>
<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
	<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">	
			<div class="cd-form floating-labels" style="max-width:1600px">
			<div style="max-width:600px; margin:0 auto;">
			<section id="crear_categoria" >							
				<fieldset >
				  <div class="alert alert-success" role="alert">
			        <strong>Felicitaciones!</strong> Tus datos se actualizaron correctamente
			      </div>
			      <p>Verificá que los datos ingresados sean correctos</p>
			      <br><br>
		          <table class="table table-striped">
		            <tbody>
		              <tr>
		                <td><b>Usuario</b></td>
		                <td><?php echo $usuario_usuario; ?></td>
		              </tr>
		              <tr>
		                <td><b>Nombre</b></td>
		                <td><?php echo $usuario_nombre; ?></td>
		              </tr>
		              <tr>
		                <td><b>Apellido</b></td>
		                <td><?php echo $usuario_apellido; ?></td>
		              </tr>
		              <tr>
		                <td><b>E-mail</b></td>
		                <td><?php echo $usuario_email; ?></td>
		              </tr>
		              <tr>
		                <td><b>Teléfono</b></td>
		                <td><?php echo $usuario_telefono; ?></td>
		              </tr>
		              <tr>
		                <td><b>Nacimiento</b></td>
		                <td><?php echo $usuario_fecha_nacimiento; ?></td>
		              </tr>	              		              
		            </tbody>
		          </table>

                <div class="row">
			        <div class="col-md-6">
			        	 <a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" href="<?php echo $Servidor_url;?>sistemaV3/00-barra-navegacion/usuario/02-modificar-datos.php">
							<span class="fa-stack fa-lg pull-left">
							  <i class="fa fa-circle fa-stack-2x"></i>
							  <i class="fa fa-history fa-stack-1x fa-inverse"></i>
							</span>
								<p>Volver</p>
						</a>
					</div>
					<div class="col-md-6">
			        	<a class="vc_btn_largo vc_btn_verde vc_btn_3d"  href="<?php echo $Servidor_url;?>sistemaV3/">
							<span class="fa-stack fa-lg pull-left">
							  <i class="fa fa-circle fa-stack-2x"></i>
							  <i class="fa fa-check fa-stack-1x fa-inverse"></i>
							</span>
								<p>Continuar</p>
						</a>
					</div>
			     </div>
		        </fieldset>	
				</section>    	
 				 </div>	
			</div>
		</div> <!-- .content-wrapper -->
	</main> 
<?php include('../../includes/pie-general.php');?>
<script src="<?php echo $Servidor_url; ?>sistemaV3/js/form.js"></script> <!-- Resource jQuery -->

</body>
</html>