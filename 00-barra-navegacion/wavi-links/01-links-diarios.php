<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

conectar2('mywavi', 'sitioweb');
//consultar en la base de datos
$query_rs_diarios = "SELECT id_diario, diario_nombre, diario_imagen, id_provincia, seccion FROM diarios ORDER BY diario_nombre ASC ";
$rs_diarios = mysql_query($query_rs_diarios)or die(mysql_error());
$row_rs_diarios = mysql_fetch_assoc($rs_diarios);
$totalrow_rs_diarios = mysql_num_rows($rs_diarios);

//consultar en la base de datos
$query_rs_links = "SELECT links_rss.id_link, links_rss.id_diario, links_rss.link_tipo, links_rss.link_dato, diarios.diario_imagen, diarios.diario_nombre, links_rss.seccion  FROM links_rss, diarios WHERE links_rss.id_diario = diarios.id_diario ORDER BY links_rss.id_link DESC";
$rs_links = mysql_query($query_rs_links)or die(mysql_error());
$row_rs_links = mysql_fetch_assoc($rs_links);
$totalrow_rs_links = mysql_num_rows($rs_links);

$array_tipo['twitter'] = "Usuario de Twitter";
$array_tipo['rss'] = "RSS";

//ultimo cargado
$query_rs_link = "SELECT id_diario, link_tipo FROM links_rss ORDER BY id_link DESC LIMIT 1";
$rs_link = mysql_query($query_rs_link)or die(mysql_error());
$row_rs_link = mysql_fetch_assoc($rs_link);
$totalrow_rs_link = mysql_num_rows($rs_link);

$ultimo_agregado_diario = $row_rs_link['id_diario'];
$ultimo_agregado_tipo = $row_rs_link['link_tipo'];


desconectar();

$url_imagen = $Servidor_url.'APLICACION/Imagenes/diarios/';
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	
	<style type="text/css">
		.tabla_encabezado {
			color: red;
		}

		tr {
			cursor: pointer;
		}

		.categorias_con_subgrupos {
			background: #f90 !important;
			color: #fff !important;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="contenedor">

				<div >					<!-- Contenido de la Pagina-->	
					<div class="cd-form floating-labels" style="max-width:1600px">
						<div style="max-width:600px; margin:0 auto;">
							<section id="crear_categoria" >							
								<fieldset >
									<form onsubmit="return validar_formulario()" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-links/php/01-links-diarios-db.php" method="POST">
										<input  type="hidden" id="elementos_contador" value="2">
										<input type="hidden" name="diario" required id="diario" value="0" >

										<legend id="txt_nueva_categoria">Agregar Links RSS</legend>

										<div id="demoBasic"  ></div>		

										<p class="cd-select">
											<select name="tipo" required class="select_class" required>
												<option value="0">Elegí un tipo</option>
												<?php foreach ($array_tipo as $clave => $valor) { 
													$selected = "";

													if($clave == $ultimo_agregado_tipo) {
														$selected = "selected";
													}
													?>
													<option selected="selected" value="<?php echo $clave; ?>"><?php echo $valor; ?></option>	
													<?php }  ?>
												</select>
											</p>

											<div class="icon">
												<label class="cd-label" for="cd-company">Links
													o dato</label>
													<input class="company" type="text" name="link" id="nueva_categoria_nombre" required>
												</div> 			    

												<div class="icon">
													<label class="cd-label" for="cd-company">Sección</label>
													<input class="company" type="text" name="seccion" id="nueva_categoria_nombre" required>
												</div> 	

												<div class="alinear_centro">
													<input type="submit" value="Continuar" id="btn_nueva_categoria">
												</div>
											</form>

										</fieldset>	
									</section>    	
								</div>
								<table class="table table-striped">
									<thead class="tabla_encabezado">
										<tr>
											<th><b>#</b></th>
											<th><b>Diario Imagen</b></th>
											<th><b>Diario Nombre</b></th>
											<th><b>Tipo</b></th>
											<th><b>Dato</b></th>
											<th><b>Sección</b></th>
										</tr>
									</thead>
									<tbody>
										<?php do {
											$id_link = $row_rs_links['id_link'];
											$id_diario = $row_rs_links['id_diario'];
											$link_tipo = $row_rs_links['link_tipo'];
											$link_dato = $row_rs_links['link_dato'];
											$diario_imagen = $row_rs_links['diario_imagen'];
											$diario_nombre = $row_rs_links['diario_nombre'];
											$seccion = $row_rs_links['seccion'];

											$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
											$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
											if($diario_imagen) {
												$imagen = $url_imagen.$diario_imagen;
												$nombre_imagen = $diario_imagen;
											}

											$super_class = null;
											if($total_categorias_vinculadas!=$total_categorias_subgrupo) {
												$super_class = 'categorias_con_subgrupos';
											}
											?>
											<tr class="<?php echo $super_class; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-links/02-ficha-link.php?link=<?php echo $id_link; ?>">
												<td><?php echo $id_link; ?></td>
												<td><img src="<?php echo $imagen; ?>"  height="40"></td>
												<td><?php echo $diario_nombre; ?></td>
												<td><?php echo $link_tipo; ?></td>
												<td><?php echo $link_dato; ?></td>
												<td><?php echo $seccion; ?></td>
											</tr>		
											<?php } while($row_rs_links = mysql_fetch_assoc($rs_links)); ?>
										</tbody>
									</table>				 

								</div>
							</div>
						</div> <!-- .content-wrapper -->
					</main> 
					<?php include('../../includes/pie-general.php');?>
					<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
					<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
					<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
					<script type="text/javascript">
						var ddData = [
						<?php
						$url_imagen = $Servidor_url.'APLICACION/Imagenes/diarios/';
						$i = 1;
						do { 
							$id_diario = $row_rs_diarios['id_diario'];
							$seccion = $row_rs_diarios['seccion'];


							$diario_nombre = $row_rs_diarios['diario_nombre'];
							$diario_imagen = $row_rs_diarios['diario_imagen'];
							$id_provincia = $row_rs_diarios['id_provincia'];


							if($diario_imagen) {
								$imagen = $url_imagen.$diario_imagen;
							} else {
								$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
							}

							$selected = "false";

							if($id_diario == $ultimo_agregado_diario) {
								$selected = "true";
							}

							$descripcion = $array_provincias[$id_provincia];
							?>
							{
								text: "<?php echo $diario_nombre; ?>",
								value: <?php echo $id_diario; ?>,
								selected: <?php echo $selected; ?>,
								description: "<?php echo $descripcion; ?>",
								imageSrc: "<?php echo $imagen; ?>"
								<?php if($i == $totalrow_rs_diarios) {
									echo '}';
								} else {
									echo '},';
								}

								$i++; } while($row_rs_diarios = mysql_fetch_assoc($rs_diarios)); ?>
								];

//Dropdown plugin data


$('#demoBasic').ddslick({
	data: ddData,
	width: 600,
	imagePosition: "left",
	selectText: "Elegí un diario",
	onSelected: function (data) {
		var valor = data.selectedData.value;
		document.getElementById("diario").value = valor;
	}
});	

function validar_formulario() {
	var diario = document.getElementById("diario").value;

	if(diario==0) {
		alert('Tenés que elegir un diario');
		return false;
	} else {
		return true;
	}
}
$('tr[data-href]').on("click", function() {
	document.location = $(this).data('href');
});
</script>
</body>
</html>