<?php 
$link = $link_dato;

function get_content($url) {
	$ch = curl_init();
	curl_setopt ($ch, CURLOPT_URL, $url);
	curl_setopt ($ch, CURLOPT_HEADER, 0);
	ob_start();
	curl_exec ($ch);
	curl_close ($ch);
	$string = ob_get_contents();
	ob_end_clean();
	return $string;
}

//traemos el contenido del link
$contenido = get_content($link);

//convertimos el xml a un array
$content = simplexml_load_string(
    $contenido
    , null
    , LIBXML_NOCDATA
);

$a = json_decode(json_encode((array) $content),1);

$array = $a['entry'];

$i=0;
$nuevo_array = null;
foreach ($array as $valor) {
	foreach ($valor as $clave2 => $valor2) {
		if($clave2=='id') {
			$nuevo_array[$i]['link'] = $valor2;
		}
		if($clave2=='title') {
			$nuevo_array[$i]['title'] = $valor2;
		}
		if($clave2=='updated') {
			$nuevo_array[$i]['date'] = $valor2;
		}
	}
	$i++;
}
?>