<?php 
$link = $link_dato;

function get_content($url) {
	$ch = curl_init();
	curl_setopt ($ch, CURLOPT_URL, $url);
	curl_setopt ($ch, CURLOPT_HEADER, 0);
	ob_start();
	curl_exec ($ch);
	curl_close ($ch);
	$string = ob_get_contents();
	ob_end_clean();
	return $string;
}

//traemos el contenido del link
$contenido = get_content($link);
//$contenido = str_replace('"', "&#34;", $contenido);
//$contenido = str_replace("'", "&#39;", $contenido);

//convertimos el xml a un arra

$content = simplexml_load_string(
    $contenido
    , null
    , LIBXML_NOCDATA
);

$a = json_decode(json_encode((array) $content),1);

$array = $a['channel']['item'];

$i=0;
$nuevo_array = null;
foreach ($array as $valor) {
	foreach ($valor as $clave2 => $valor2) {
		if($clave2=='title') {
			$nuevo_array[$i]['title'] = $valor2;
		}
		if($clave2=='link') {
			$nuevo_array[$i]['link'] = $valor2;
		}
		if($clave2=='pubDate') {
			$nuevo_array[$i]['date'] = $valor2;
		}

	}
	$i++;
}
?>