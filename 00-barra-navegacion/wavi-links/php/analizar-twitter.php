<?php 
$link = 'https://twitrss.me/twitter_user_to_rss/?user='.$link_dato;

function get_content($url) {
	$ch = curl_init();
	curl_setopt ($ch, CURLOPT_URL, $url);
	curl_setopt ($ch, CURLOPT_HEADER, 0);
	ob_start();
	curl_exec ($ch);
	curl_close ($ch);
	$string = ob_get_contents();
	ob_end_clean();
	return $string;
}

//traemos el contenido del link
$contenido = get_content($link);

//convertimos el xml a un array
$a = json_decode(json_encode((array) simplexml_load_string($contenido)),1);

$array = $a['channel']['item'];

$i=0;
foreach ($array as $valor) {
	foreach ($valor as $clave2 => $valor2) {
		if($clave2=='title') {
			$explorar = explode('http://', $valor2);
			$nuevo_array[$i]['title'] = trim($explorar[0]);

			$agregar = 'http://';

			if(!$explorar[1]) {
				$explorar = explode('https://', $valor2);
				$nuevo_array[$i]['title'] = trim($explorar[0]);
				$agregar = 'https://';
			}

			$link1 = utf8_encode($explorar[1]);
			$explorar2 = explode('Â', $link1);

			$link = null;
			if($explorar2[0]) {
				$link=$agregar.trim($explorar2[0]);
			} 
			//$link = substr($link, 0, -2);

			$nuevo_array[$i]['link'] = $link;
		}
		if($clave2=='pubDate') {
			$nuevo_array[$i]['date'] = trim($valor2);
		}
	}
	$i++;
}
?>