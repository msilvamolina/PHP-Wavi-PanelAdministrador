<?php include('../paginas_include/variables-generales.php'); 
$permisos_pagina = '';
include('php/verificar-permisos.php');

$url_panel = $Servidor_url.'PANELADMINISTRADOR/';
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('includes/head-general.php'); ?>
	<style type="text/css">
		.mensaje { 
			margin: 50px auto;
			width: 100%;
			max-width: 550px;
		 }
	</style>
</head>
<body>
	<?php include('includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<center>
				<img src="<?php echo $url_panel; ?>img/mensaje-bienvenida.png" class="mensaje">
			</center>
		</div> <!-- .content-wrapper -->
	</main> <!-- .cd-main-content -->
<script src="<?php echo $Servidor_url; ?>jquery.js"></script>
<script src="<?php echo $url_panel; ?>js/jquery.menu-aim.js"></script>
<script src="<?php echo $url_panel; ?>js/main.js"></script> <!-- Resource jQuery -->
</body>
</html>