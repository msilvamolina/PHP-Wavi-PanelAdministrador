<?php 
function comprobar_active($carpeta) {
	$url = $_SERVER['REQUEST_URI']; 
	$explorar_url = explode('/', $url);
	$url_actual = $explorar_url[3];
	$return=NULL;
	if($url_actual==$carpeta) {
		$return = 'active';
	}
	return $return;
}

$total_reportes = $_SESSION['total_reportes'];
$total_negocios_verdes = $_SESSION['total_negocios_verdes'];

?>
<nav class="cd-side-nav">
	<ul>
		<li class="cd-label">General aplicación</li>
		<?php $active = comprobar_active('notificaciones'); ?>		
		<li class="has-children notifications <?php echo $active; ?>">
			<a href="#0">Notificaciones<span class="count">3</span></a>
			
			<ul>
				<li><a href="#0">Todas las notificaciones</a></li>
			</ul>
		</li>

		<li class="cd-label">Cuenta</li>
		<?php $active = comprobar_active('usuario'); ?>		
		<li class="has-children <?php echo $active; ?>">
			<a href="#0">Usuario</a>
		<!-- ejemplo de como seria con un contador al lado
			<a href="#0">Guía de Servicios<span class="count">3</span></a>
		-->	
		<ul>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/usuario/01-cambiar-contrasena.php">Cambiar Contraseña</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/usuario/02-modificar-datos.php">Modificar Datos</a></li>
		</ul>
	</li>
	<li class="cd-label">Aplicación</li>
	<?php $active = comprobar_active('wavi-general'); ?>		
	<li class="has-children overview <?php echo $active; ?>">
		<a href="#0">Wavi General</a>
		<ul>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/01-version-app.php">Versión App</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/09-emulador-categorias.php">Emulador categorías</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/02-grupos-categorias.php">Grupos de categorías</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/03-administrar-grupos-categorias.php">Administrar grupos de categorías</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/04-ciudades.php">Ciudades y provincias</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/05-negocios-reportados.php">Negocios Reportados</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/06-mandar-notificacion-wavi.php">Mandar Notificación</a></li>
		</ul>
	</li>
	<?php $active = comprobar_active('wavi-negocios'); ?>		
	<li class="has-children comments <?php echo $active; ?>">
		<a href="#0">Wavi Negocios</a>
		<ul>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/01-negocios-sin-conexion.php">Negocios sin conexión</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/02-cargar-negocio-paso3-1.php">Cargar Negocio</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios.php">Negocios</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/07-mover-negocios.php">Mover Negocios</a></li>
		</ul>
	</li>
	<?php $active = comprobar_active('wavi-noticias'); ?>		
	<li class="has-children bookmarks <?php echo $active; ?>">
		<a href="#0">Wavi Noticias</a>
		<ul>				
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/05-diarios.php">Diarios</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/00-cargar-nota.php">Cargar nota</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/04-noticias.php">Notas</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/08-agrupar-notas.php">Agrupar notas</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/10-ordenar-grupos.php">Ordenar grupos</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/09-ordenar-notas.php">Ordenar notas</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/11-armar-json-noticias.php">Armar json noticias</a></li>
		</ul>
	</li>
	<?php $active = comprobar_active('wavi-links'); ?>		
	<li class="has-children bookmarks <?php echo $active; ?>">
		<a href="#0">Wavi Links</a>
		<ul>				
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-links/01-links-diarios.php">Links Diarios</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-links/04-ejecutar-links.php">Ejecutar Links</a></li>
		</ul>
	</li>			
	<?php $active = comprobar_active('wavi-promos'); ?>		
	<li class="has-children bookmarks <?php echo $active; ?>">
		<a href="#0">Wavi Promos</a>
		<ul>				
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/01-promociones.php">Promociones</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-promos/02-crear-promocion.php">Crear Promoción</a></li>
		</ul>
	</li>		
</ul>
</nav>