<?php
//estos son los permisos que se van a mostrar y guardar en la base de datos
//se escriben aca por única vez y todo el sistema se adapta a esto
$permisos_mostrar['agencia'] = 'Agencia';
$permisos_mostrar['ver_configuracion'] = 'Ver Configuración';
$permisos_mostrar['recibir_notificaciones'] = 'Recibir Notificaciones';
$permisos_mostrar['recordarse'] = 'Recordarse en el Sistema';
$permisos_mostrar['herramientas_administrativas'] = 'Herramientas Administrativas';
$permisos_mostrar['placas'] = 'Placas';
$permisos_mostrar['micros_municipalidad'] = 'Micros Municipalidad';
$permisos_mostrar['ultimos_movimientos'] = 'Últimos Movimientos';
$permisos_mostrar['robot_general'] = 'Robot General';
$permisos_mostrar['revista_mbp'] = 'Revista MBP';
$permisos_mostrar['revista_mbp_editor'] = 'Revista MBP Editor';
$permisos_mostrar['revista_mbp_webmaster'] = 'Revista MBP Webmaster';
$permisos_mostrar['soynoticia'] = 'Soy Noticia';
$permisos_mostrar['manzone'] = 'Sandra Manzone';
$permisos_mostrar['aplicaciones_diarios'] = 'Aplicaciones Diarios';
$permisos_mostrar['aplicaciones_diarios_webmaster'] = 'Aplicaciones Diarios Webmaster';
$permisos_mostrar['tarjeta_ciudadana'] = 'Tarjeta Ciudadana';
$permisos_mostrar['aplicaciones_politicos'] = 'Aplicaciones Políticos';
$permisos_mostrar['herramientas_webmaster'] = 'Herramientas Webmaster';
$permisos_mostrar['herramientas_borrar'] = 'Herramientas Borrar';
$permisos_mostrar['herramientas_restaurar'] = 'Herramientas Restaurar';
?>